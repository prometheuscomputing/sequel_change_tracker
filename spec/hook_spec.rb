require File.dirname(__FILE__) + '/spec_helper.rb'

describe DomainObject, " when setting hooks on change tracked models" do
  before(:each) do
    clear_db
    ChangeTracker.cancel
    ChangeTracker.start
    @series = Series.create(:label => "Russian Lit")
    @book1 = Book.create(:title => "War and Peace")
    @book2 = Book.create(:title => "Crime and Punishment")
    @book3 = Book.create(:title => "The Brothers Karamazov")
    @book4 = Book.create(:title => "Invalid Title")
    @library = Library.create(:name => "WCU public library")
    ChangeTracker.commit
  end

  it "should process save hooks when saving a simple derived attribute" do
    ChangeTracker.start
    @series.add_book @book1
    @series.add_book @book2
    @series.add_book @book3
    @book1.save
    @series.save
    
    @series.book_names.should == 'War and Peace; Crime and Punishment; The Brothers Karamazov'
    
    ChangeTracker.commit
    
    @series.book_names.should == 'War and Peace; Crime and Punishment; The Brothers Karamazov'

    # Test that value is stored properly in DB
    series_from_db = Series[@series.id]
    series_from_db.book_names.should == 'War and Peace; Crime and Punishment; The Brothers Karamazov'
    
    ChangeTracker.start
    
    lambda { @series.add_book @book4; @series.save }.should raise_error(/Bad Book!/)
  end
  
  it "creation hooks should only be processed on creation" do
    ChangeTracker.start
    @book1.save
    ChangeTracker.commit
    @book1.catalog_number.should == "To Be Cataloged"
    @book1.number_of_pages.should == "Unknown"
    ChangeTracker.start
    @book1.catalog_number = '19.345.33L'
    @book1.number_of_pages = '555'
    ChangeTracker.commit
    @book1.catalog_number.should == '19.345.33L'
    @book1.number_of_pages.should == '555'
  end
  
  it "should process save hooks when saving a complicated derived attribute" do
    ChangeTracker.start
    # Save the books to set their new derived attribute
    @book1.save
    @book2.save
    @book3.save
    ChangeTracker.commit
    
    ChangeTracker.start
    @series.add_book @book1
    
    
    @series.add_book @book2
    @series.add_book @book3
    
    @series.short_book_names.should == 'WaP; CaP; TBK'
    
    ChangeTracker.commit
    
    @series.short_book_names.should == 'WaP; CaP; TBK'
    # Test that value is stored properly in DB
    series_from_db = Series[@series.id]
    series_from_db.short_book_names.should == 'WaP; CaP; TBK'
  end
  
  it "should be able to manipulate attributes and associations in the before_change_tracker_save hook (without saving first)" do
    ChangeTracker.start
    @library.add_book @book1
    pending "implementation of before_change_tracker_save that will allow manipulation of associations"
    @library.number_of_books.should == 1
    ChangeTracker.commit
    @library.number_of_books.should == 1
    Library[@library.id].number_of_books.should == 1
  end
end