begin
  require 'rspec'
rescue LoadError
  require 'rubygems' unless ENV['NO_RUBYGEMS']
  gem 'rspec'
  require 'rspec'
end

# Enable old syntax for Rspec 3.0
RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end

$DEBUG = false

require 'rubygems'
require 'sequel'

db_file = File.dirname(__FILE__) + '/test_sql.db'
File.delete(db_file) if File.exists?(db_file) && ! File.directory?(db_file)
if defined? Java
  require File.join(File.dirname(__FILE__), %w[sqlitejdbc-v056.jar])
  DB = Sequel.connect("jdbc:sqlite:#{db_file}")
else  
  DB = Sequel.sqlite(db_file)
  DB.foreign_keys = false
end
#Sequel.datetime_class = DateTime #Change from default: Time

puts "Requiring Specific Associations"
begin
  # Look for Specific Associations as a sibling project to Change Tracker
  local_ssa_path = File.expand_path('../../../sequel_specific_associations/lib', __FILE__)
  puts "Checking for SSA at: #{local_ssa_path}"
  $:.unshift local_ssa_path
  require local_ssa_path + '/sequel_specific_associations'
  puts "USING LOCAL COPY OF SPECIFIC ASSOCIATIONS".center(100, "-")
  puts "Found at #{local_ssa_path}".center(100, "-")
rescue LoadError => e
  $:.shift # Remove local_ssa_path from load path
  puts "Couldn't load local copy of SSA: #{e.message}"
  require 'sequel_specific_associations'
end

$:.unshift(File.dirname(__FILE__) + '/../lib')
require 'sequel_change_tracker'

def setup_session
  $session = {:username => 'username', :sequel_change_tracker => {Thread.current.object_id => {}}}
  ChangeTracker.session_hash = $session
end
setup_session

require 'usecasemetamodelv4_schema.rb' unless DB.table_exists?(:actors)
require 'usecasemetamodelv4_sequel.rb'
require_relative 'woods.rb'
require 'ordered_example.rb'

# Load carexample into test database
$DB_HASH = {}
$DB_HASH['CarExample'] = DB
require 'car_example_generated'
require CarExampleGenerated.driver_file
require 'gui_director/options'
require 'gui_director/model_extensions'
require 'Foundation/load_path_management'
require 'car_example/model_extensions'

#ChangeTracker.set_session_hash($session)


class DomainObject < Sequel::Model
  plugin :change_tracker #, $session[:username], $session[:current_uow]
end

include UseCaseMetaModelV4

# Initialize the change_tracker plugin


def clear_db
  DB.tables.each do |table|
    DB[table].delete
  end
  true
end


# These are for ease of console testing only
def s
  ChangeTracker.start
end

def c
  ChangeTracker.commit
end