class Library < Sequel::Model
  plugin :specific_associations
  plugin :change_tracker
  attribute :name, String
  attribute :number_of_books, Numeric
  one_to_many :Book, :order => true, :other_prefix => :books
  many_to_many :Patron, :order => true, :through => :PatronStatusAtLibrary
  
  def before_change_tracker_save
    self[:number_of_books] = self.books.count
    super
  end
end

class PatronStatusAtLibrary < Sequel::Model
  plugin :specific_associations
  plugin :change_tracker
  attribute :status, String
  associates :Library, :is_ordered => true; associates :Patron, :is_ordered => true
end

class Book < Sequel::Model
  plugin :specific_associations
  plugin :change_tracker
  attribute :title, String
  attribute :short_title, String
  attribute :catalog_number, String
  attribute :number_of_pages, String
  many_to_one :Library, :is_ordered => true
  many_to_many :Series, :order => true, :is_ordered => true, :through => :BooksInSeries, :prefix => :books, :other_prefix => :series
  
  def after_change_tracker_save
    super
    short_title = self.title.split(' ').collect{|t| t[0]}.join if self.title && !self.title.empty?

    # Update the derived column: short_title
    unless self[:short_title] == short_title
      self[:short_title] = short_title
      self.save # Save this book again, since we've made changes.
      return # This hook will be called again by the above #save call, so no need to continue.
    end
    # Update other associations with derived attributes depending on short_title
    self.series.each do |s|
      s.update_short_names(self)
      s.save
    end
  end
  
  
  # Somewhat contrived because it will override anything that was set before the first save but fine for testing purposes
  def before_change_tracker_create
    super
    self[:number_of_pages] = "Unknown"
  end
  
  # Somewhat contrived because it will override anything that was set before the first save but fine for testing purposes
  def after_change_tracker_create
    super
    self[:catalog_number] = "To Be Cataloged"
  end
end

class Dictionary < Book
  set_dataset :dictionaries
end

class Patron < Sequel::Model
  plugin :specific_associations
  plugin :change_tracker
  attribute :first_name, String
  many_to_many :Library, :is_ordered => true, :through => :PatronStatusAtLibrary
end

class BooksInSeries < Sequel::Model
  plugin :specific_associations
  plugin :change_tracker
  association_class!
  associates :Book => :book, :is_ordered => true; associates :Series => :series, :is_ordered => true
end

class Series < Sequel::Model
  plugin :specific_associations
  plugin :change_tracker
  attribute :label, String
  attribute :book_names, String
  attribute :short_book_names, String
  many_to_many :Book, :order => true, :is_ordered => true, :through => :BooksInSeries, :prefix => :series, :other_prefix => :books
  

  def before_change_tracker_save
    self[:book_names] = self.books.collect{|b| b.title}.join('; ')
    super
  end
  
  def after_change_tracker_save
    super
    raise "Bad Book!" if self.books.any? {|b| b.title == 'Invalid Title'}
  end
  
  def update_short_names(updated_book = nil)
    books = self.books
    if updated_book
      index = books.index{|b| b.id == updated_book.id}
      books[index] = updated_book
    end
    short_book_names = books.collect{|b| b[:short_title]}.join('; ')
    self[:short_book_names] = short_book_names unless self[:short_book_names] == short_book_names
    short_book_names
  end
end

Library.create_schema
PatronStatusAtLibrary.create_schema
Book.create_schema
Dictionary.create_schema
Patron.create_schema
BooksInSeries.create_schema
Series.create_schema