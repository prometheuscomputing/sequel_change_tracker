module Woods
  # # ============= Begin class Unit_Of_Work =============
  # class Unit_Of_Work < Sequel::Model
  #   set_dataset :unit_of_works
  #   plugin :boolean_readers
  #   plugin :specific_associations
  #   plugin :change_tracker
  #   one_to_many :Step, :polymorphic => true
  #
  #
  # end
  # # ============= End class Unit_Of_Work =============
  class Bear < Sequel::Model
    set_dataset :bears
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    attribute :name, String
    many_to_one :'Woods::FurColor', :getter => :fur_color, :opp_getter => :bears_with_fur_color, :singular_opp_getter => :bears_with_fur_color, :enumeration => true
    many_to_one :'Woods::Gender', :getter => :gender, :opp_getter => :bears_with_gender, :singular_opp_getter => :bears_with_gender, :enumeration => true
  end
      
  class FurColor < Sequel::Model
    set_dataset :fur_colors
    plugin :specific_associations; plugin :timestamps; plugin :boolean_readers; plugin :change_tracker
    enumeration!
    attribute :value, String
    one_to_many :'Woods::Bear', :getter => :bears_with_fur_color, :opp_getter => :fur_color, :singular_opp_getter => :fur_color
  end
  
  class Gender < Sequel::Model
    set_dataset :genders
    plugin :specific_associations; plugin :timestamps; plugin :boolean_readers; plugin :change_tracker
    enumeration!
    attribute :value, String
    one_to_many :'Woods::Bear', :getter => :bears_with_gender, :opp_getter => :gender, :singular_opp_getter => :gender
  end
end
Woods::Bear.create_schema?
Woods::FurColor.create_schema?
Woods::Gender.create_schema?
