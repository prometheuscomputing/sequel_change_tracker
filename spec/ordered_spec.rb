require File.dirname(__FILE__) + '/spec_helper.rb'

describe DomainObject, " for a one to many ordered association" do
  before(:each) do
    clear_db
    ChangeTracker.cancel
    ChangeTracker.start
    @book1 = Book.create(:title => "War and Peace")
    @book2 = Book.create(:title => "Crime and Punishment")
    @book3 = Book.create(:title => "The Brothers Karamazov")
    @library = Library.create(:name => "WCU public library")
    ChangeTracker.commit
  end

  it "should add new associations to the end of the associations list" do
    ChangeTracker.start
    @library.add_book(@book1)
    ChangeTracker.commit
    @library.books.count.should == 1
    @library.books.first.should be_an_instance_of(Book)
    @library.books.first.title.should == "War and Peace"
    # Check position
    @book1.library_position.should == 0
    
    ChangeTracker.start
    @library.add_book(@book2)
    ChangeTracker.commit
    @library.books.count.should == 2
    @library.books.first.title.should == @book1.title
    @library.books.last.title.should == @book2.title
    # Check positions
    @book1.library_position.should == 0
    @book2.library_position.should == 1
    
    ChangeTracker.start
    @library.add_book(@book3)
    ChangeTracker.commit
    @library.books.count.should == 3
    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book2.title
    @library.books[2].title.should == @book3.title
    # Check positions
    @book1.library_position.should == 0
    @book2.library_position.should == 1
    @book3.library_position.should == 2
    
    # Remove book2
    ChangeTracker.start
    @library.remove_book(@book2)
    ChangeTracker.commit
    @library.books.count.should == 2
    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book3.title
    # Check positions
    @book1.library_position.should == 0
    # Must refresh objects that are indirectly modified
    @book3.refresh.library_position.should == 1
    
    # Re-add book2 (should go to end of list)
    ChangeTracker.start
    @library.add_book(@book2)
    ChangeTracker.commit
    @library.books.count.should == 3
  

    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book3.title
    @library.books[2].title.should == @book2.title
    # Check positions
    @book1.library_position.should == 0
    @book3.library_position.should == 1
    @book2.library_position.should == 2
  end
  
  it "should reorder remaining associations when an association is removed" do
    ChangeTracker.start
    @library.add_book(@book1)
    @library.add_book(@book2)
    @library.add_book(@book3)
    ChangeTracker.commit
    
    @library.books[0].id.should == @book1.id
    @library.books[1].id.should == @book2.id
    @library.books[2].id.should == @book3.id
    
    # Remove book2
    ChangeTracker.start
    @library.remove_book(@book2)
    ChangeTracker.commit
    # Check number of elements
    @library.books.count.should == 2
    # Check array positions
    @library.books[0].id.should == @book1.id
    @library.books[1].id.should == @book3.id
    # Check position columns
    @library.books[0].library_position.should == 0
    @library.books[1].library_position.should == 1
    
    # Remove book3
    ChangeTracker.start
    @library.remove_book(@book3.refresh)
    ChangeTracker.commit
    # Check number of elements
    @library.books.count.should == 1
    # Check array positions
    @library.books[0].id.should == @book1.id
    # Check position columns
    @library.books[0].library_position.should == 0
  end
  
  
  it "should allow the associations to be reordered to specific positions" do
    # This test assumes that when moving books, the books after the new position of the moved book are
    # moved back one position (stopping if/when the old position of the moved book is reached)
    ChangeTracker.start
    @library.add_book(@book1)
    @library.add_book(@book2)
    ChangeTracker.commit
    @library.books.count.should == 2
    @library.books.last.title.should == @book2.title
    
    ChangeTracker.start
    @library.move_book(@book2, 0)
    ChangeTracker.commit
    @library.books.count.should == 2
    # Books should now be reversed
    @library.books.first.title.should == @book2.title
    @library.books.last.title.should == @book1.title
    
    ChangeTracker.start
    @library.move_book(@book1, 0)
    ChangeTracker.commit
    @library.books.count.should == 2
    # Books should be reversed to original positions
    @library.books.first.title.should == @book1.title
    @library.books.last.title.should == @book2.title
    
    # Add a third book to the library
    ChangeTracker.start
    @library.add_book(@book3)
    ChangeTracker.commit
    @library.books.count.should == 3
    @library.books.last.title.should == @book3.title
    
    # Move the third book to the middle position
    ChangeTracker.start
    @library.move_book(@book3, 1)
    ChangeTracker.commit
    @library.books.count.should == 3
    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book3.title
    @library.books[2].title.should == @book2.title
    
    # Move the second book to the first position
    ChangeTracker.start
    @library.move_book(@book2, 0)
    ChangeTracker.commit
    @library.books.count.should == 3
    @library.books[0].title.should == @book2.title
    @library.books[1].title.should == @book1.title
    @library.books[2].title.should == @book3.title
    
    # Move the first book to the first position
    ChangeTracker.start
    @library.move_book(@book1, 0)
    ChangeTracker.commit
    @library.books.count.should == 3
    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book2.title
    @library.books[2].title.should == @book3.title
  end
  
  it "should raise an ArgumentError on invalid input to the move command" do
    ChangeTracker.start
    @library.add_book(@book1)
    @library.add_book(@book2)
    ChangeTracker.commit
    
    ChangeTracker.start
    # Invalid input: @book3 is not part of the collection
    lambda { @library.move_book(@book3, 0) }.should raise_error(ArgumentError)
    
    # Invalid input: nil is not a valid item
    lambda { @library.move_book(nil, 0) }.should raise_error(ArgumentError)
    
    # Invalid input: true is not a valid item
    lambda { @library.move_book(true, 0) }.should raise_error(ArgumentError)
    
    # Invalid input: nil is not a valid position
    lambda { @library.move_book(@book2, nil) }.should raise_error(ArgumentError)
  end

  it "should allow swapping associations' positions" do
    ChangeTracker.start
    @library.add_book(@book1)
    @library.add_book(@book2)
    ChangeTracker.commit
    @library.books.last.title.should == @book2.title
    
    # Swap the books
    ChangeTracker.start
    pending "Implementation of the swap_ method"
    @library.swap_books(@book1, @book2)
    ChangeTracker.commit
    @library.books.first.title.should == @book2.title
    @library.books.last.title.should == @book1.title
  
    # Add a third book
    ChangeTracker.start
    @library.add_book(@book3)
    ChangeTracker.commit
    
    # Check ordering
    @library.books[0].title.should == @book2.title
    @library.books[1].title.should == @book1.title
    @library.books[2].title.should == @book3.title
    
    # Swap book3 with book2
    ChangeTracker.start
    @library.swap_books(@book3, @book2)
    ChangeTracker.commit
    @library.books[0].title.should == @book3.title
    @library.books[1].title.should == @book1.title
    @library.books[2].title.should == @book2.title
    
    # Swap book3 with book1
    ChangeTracker.start
    @library.swap_books(@book3, @book1)
    ChangeTracker.commit
    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book3.title
    @library.books[2].title.should == @book2.title
  end
end
    
  
describe DomainObject, " for a many to many ordered association" do
  before(:each) do
    clear_db
    ChangeTracker.start
    @library1 = Library.create(:name => "WCU public library")
    @library2 = Library.create(:name => "Sylva public library")
    @library3 = Library.create(:name => "Franklin public library")
    @patron1 = Patron.create(:first_name => "Bob")
    @patron2 = Patron.create(:first_name => "Sue")
    @patron3 = Patron.create(:first_name => "Bill")
    ChangeTracker.commit
  end

  it "should add new associations to the end of the associations list" do
    ChangeTracker.start
    @library1.add_patron(@patron1)
    ChangeTracker.commit
    @library1.patrons.count.should == 1
    @library1.patrons.first.should be_an_instance_of(Patron)
    @library1.patrons.first.first_name.should == "Bob"
    
    ChangeTracker.start
    @library1.add_patron(@patron2)
    ChangeTracker.commit
    @library1.patrons.count.should == 2
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron2.id
    
    ChangeTracker.start
    @library1.add_patron(@patron3)
    ChangeTracker.commit
    @library1.patrons.count.should == 3
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron2.id
    @library1.patrons[2].id.should == @patron3.id
  end
  
  it "should reorder remaining associations when an association is removed" do
    ChangeTracker.start
    @library1.add_patron(@patron1)
    @library1.add_patron(@patron2)
    @library1.add_patron(@patron3)
    ChangeTracker.commit
    
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron2.id
    @library1.patrons[2].id.should == @patron3.id
      
    # Remove patron2
    ChangeTracker.start
    @library1.remove_patron(@patron2)
    ChangeTracker.commit
    # Check number of elements
    @library1.patrons.count.should == 2
    # Check array positions
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron3.id
    # Check position columns
    @library1.patrons_records[0].library_position.should == 0
    @library1.patrons_records[1].library_position.should == 1
    
    # Remove patron3
    ChangeTracker.start
    @library1.remove_patron(@patron3)
    ChangeTracker.commit
    # Check number of elements
    @library1.patrons.count.should == 1
    # Check array positions
    @library1.patrons[0].id.should == @patron1.id
    # Check position columns
    @library1.patrons_records[0].library_position.should == 0
  end
  
  it "should allow the associations to be reordered to specific positions" do
    # This test assumes that when moving items, the items after the new position of the moved item are
    # moved back one position (stopping if/when the old position of the moved item is reached)
    ChangeTracker.start
    @library1.add_patron(@patron1)
    @library1.add_patron(@patron2)
    ChangeTracker.commit
    @library1.patrons.last.id.should == @patron2.id
    
    ChangeTracker.start
    @library1.move_patron(@patron2, 0)
    ChangeTracker.commit
    # Should now be reversed
    @library1.patrons[0].id.should == @patron2.id
    @library1.patrons[1].id.should == @patron1.id
    
    ChangeTracker.start
    @library1.move_patron(@patron1, 0)
    ChangeTracker.commit
    # Should be reversed to original positions
    @library1.patrons.first.id.should == @patron1.id
    @library1.patrons.last.id.should == @patron2.id
    
    # Add a third patron to the library
    ChangeTracker.start
    @library1.add_patron(@patron3)
    ChangeTracker.commit
    @library1.patrons.last.id.should == @patron3.id
    
    # Move the third patron to the middle position
    ChangeTracker.start
    @library1.move_patron(@patron3, 1)
    ChangeTracker.commit
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron3.id
    @library1.patrons[2].id.should == @patron2.id
    
    # Move the second patron to the first position
    ChangeTracker.start
    @library1.move_patron(@patron2, 0)
    ChangeTracker.commit
    @library1.patrons[0].id.should == @patron2.id
    @library1.patrons[1].id.should == @patron1.id
    @library1.patrons[2].id.should == @patron3.id
    
    # Move the first patron to the first position
    ChangeTracker.start
    @library1.move_patron(@patron1, 0)
    ChangeTracker.commit
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron2.id
    @library1.patrons[2].id.should == @patron3.id
  end
  
  it "should allow swapping associations' positions" do
    ChangeTracker.start
    @library1.add_patron(@patron1)
    @library1.add_patron(@patron2)
    ChangeTracker.commit
    @library1.patrons.last.id.should == @patron2.id
    
    # Swap the patrons
    ChangeTracker.start
    pending "Implementation of the swap_ method"
    @library1.swap_patrons(@patron1, @patron2)
    ChangeTracker.commit
    @library1.patrons.first.id.should == @patron2.id
    @library1.patrons.last.id.should == @patron1.id
  
    # Add a third patron
    ChangeTracker.start
    @library1.add_patron(@patron3)
    ChangeTracker.commit
    
    # Check ordering
    @library1.patrons[0].id.should == @patron2.id
    @library1.patrons[1].id.should == @patron1.id
    @library1.patrons[2].id.should == @patron3.id
    
    # Swap patron3 with patron2
    ChangeTracker.start
    @library1.swap_patrons(@patron3, @patron2)
    ChangeTracker.commit
    @library1.patrons[0].id.should == @patron3.id
    @library1.patrons[1].id.should == @patron1.id
    @library1.patrons[2].id.should == @patron2.id
    
    # Swap patron3 with patron1
    ChangeTracker.start
    @library1.swap_patrons(@patron3, @patron1)
    ChangeTracker.commit
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron3.id
    @library1.patrons[2].id.should == @patron2.id
  end
end


describe DomainObject, " for a many to many ordered association where both directions are ordered" do
  before(:each) do
    clear_db
    ChangeTracker.start
    @book1 = Book.create(:title => "War and Peace")
    @book2 = Book.create(:title => "Crime and Punishment")
    @book3 = Book.create(:title => "The Brothers Karamazov")
    
    @book4 = Dictionary.create(:title => "Websters")
    @book5 = Dictionary.create(:title => "Oxford")
    @book6 = Dictionary.create(:title => "Russian Dictionary")
    
    @series1 = Series.create(:label => "Russian Lit")
    @series2 = Series.create(:label => "Dictionaries")
    @series3 = Series.create(:label => "Favorites")
    ChangeTracker.commit
  end
  
  # Default Order guide::
  
  # Russian Lit1      Dictionaries2       Favorites3
  # book1             book4               book4
  # book2             book5               book1
  # book3             book6               book6
  # book6
  
  # Book1             Book2           Book3           Book4           Book5           Book6
  # Russian Lit1      Russian Lit1    Russian Lit1    Dictionaries2   Dictionaries2   Favorites3
  # Favorites3                                        Favorites3                      Dictionaries2
  #                                                                                   Russian Lit1
  
  it "should set positions for both sides of the associations when adding an element" do
    ChangeTracker.start
    @series1.add_book(@book1)
    ChangeTracker.commit
    @series1.books.count.should == 1
    @book1.series.count.should == 1
    @series1.books[0].id.should == @book1.id
    @book1.series[0].id.should == @series1.id
    @series1.books_records[0].book.id.should == @book1.id
    @book1.series_records[0].series.id.should == @series1.id
    @series1.books_records[0].series_position.should == 0
    @book1.series_records[0].book_position.should == 0
    
    ChangeTracker.start
    @series1.add_book(@book2)
    ChangeTracker.commit
    @series1.books.count.should == 2
    @book2.series.count.should == 1
    @series1.books[0].id.should == @book1.id
    @series1.books[1].id.should == @book2.id
    @book2.series[0].id.should == @series1.id
    @series1.books_records[0].book.id.should == @book1.id
    @series1.books_records[1].book.id.should == @book2.id
    @book2.series_records[0].series.id.should == @series1.id
    @series1.books_records[0].series_position.should == 0
    @series1.books_records[1].series_position.should == 1
    @book2.series_records[0].book_position.should == 0
  end
  
  it "should add new associations to the end of the associations list" do
    ChangeTracker.start
    @series1.add_book(@book1)
    @series1.add_book(@book2)
    @series1.add_book(@book3)
    ChangeTracker.commit

    @series1.books.count.should == 3
    @series1.books[0].id.should == @book1.id
    @series1.books[1].id.should == @book2.id
    @series1.books[2].id.should == @book3.id

    ChangeTracker.start
    @series2.add_book(@book4)
    @series2.add_book(@book5)
    @series2.save
    ChangeTracker.commit
    
    @series2.books.count.should == 2
    @series2.books[0].id.should == @book4.id
    @series2.books[1].id.should == @book5.id
    
    ChangeTracker.start
    @series3.add_book(@book4)
    @series3.add_book(@book1)
    @series3.add_book(@book6)
    ChangeTracker.commit
    
    @series3.books.count.should == 3
    @series3.books[0].id.should == @book4.id
    @series3.books[1].id.should == @book1.id
    @series3.books[2].id.should == @book6.id
    
    ChangeTracker.start
    @series2.refresh
    @series2.add_book(@book6)
    ChangeTracker.commit
    @series2.books.count.should == 3
    @series2.books[0].id.should == @book4.id
    @series2.books[1].id.should == @book5.id
    @series2.books[2].id.should == @book6.id
    
    ChangeTracker.start
    @series1.refresh
    @series1.add_book(@book6)
    ChangeTracker.commit
    @series1.books.count.should == 4
    @series1.books[0].id.should == @book1.id
    @series1.books[1].id.should == @book2.id
    @series1.books[2].id.should == @book3.id
    @series1.books[3].id.should == @book6.id
    
    # Test from opposite end
    @book1.series.count.should == 2
    @book1.series[0].id.should == @series1.id
    @book1.series[1].id.should == @series3.id
    
    @book2.series.count.should == 1
    @book2.series[0].id.should == @series1.id
    
    @book3.series.count.should == 1
    @book3.series[0].id.should == @series1.id
    
    @book4.series.count.should == 2
    @book4.series[0].id.should == @series2.id
    @book4.series[1].id.should == @series3.id
    
    @book5.series.count.should == 1
    @book5.series[0].id.should == @series2.id
    
    @book6.series.count.should == 3
    @book6.series[0].id.should == @series3.id
    @book6.series[1].id.should == @series2.id
    @book6.series[2].id.should == @series1.id
    
  end
  
  it "should reorder remaining associations when an association is removed" do
    ChangeTracker.start
    @series1.add_book(@book1)
    @series1.add_book(@book2)
    @series1.add_book(@book3)
    @series2.add_book(@book4)
    @series2.add_book(@book5)
    @series3.add_book(@book4)
    @series3.add_book(@book1)
    @series3.add_book(@book6)
    @series2.add_book(@book6)
    @series1.add_book(@book6)
    ChangeTracker.commit
    
    ChangeTracker.start
    @series3.remove_book(@book4)
    ChangeTracker.commit
    
    # Check series ordering
    @series3.books.count.should == 2
    @series3.books[0].id.should == @book1.id
    @series3.books[1].id.should == @book6.id
    # Check position column
    @series3.books_records[0].book.id.should == @book1.id
    @series3.books_records[0].series_position.should == 0
    @series3.books_records[1].book.id.should == @book6.id
    @series3.books_records[1].series_position.should == 1
    
    # Check book ordering
    @book4.series.count.should == 1
    @book4.series[0].id.should == @series2.id
    # Check position column
    @book4.series_records[0].series.id.should == @series2.id
    @book4.series_records[0].book_position.should == 0
    
    
    ChangeTracker.start
    @book1.remove_series(@series1)
    ChangeTracker.commit
    
    # Check book ordering
    @book1.series.count.should == 1
    @book1.series[0].id.should == @series3.id
    # Check position column
    @book1.series_records[0].series.id.should == @series3.id
    @book1.series_records[0].book_position.should == 0
    
    # Check series ordering
    @series1.books.count.should == 3
    @series1.books[0].id.should == @book2.id
    @series1.books[1].id.should == @book3.id
    @series1.books[2].id.should == @book6.id
    # Check position column
    @series1.books_records[0].book.id.should == @book2.id
    @series1.books_records[0].series_position.should == 0
    @series1.books_records[1].book.id.should == @book3.id
    @series1.books_records[1].series_position.should == 1
    @series1.books_records[2].book.id.should == @book6.id
    @series1.books_records[2].series_position.should == 2
  end
  
  it "should be able to set the association from an association class" do
    ChangeTracker.start
    @series1.add_book(@book1)
    @series1.add_book(@book2)
    @series1.add_book(@book3)
    @series2.add_book(@book4)
    @series2.add_book(@book5)
    @series3.add_book(@book4)
    @series3.add_book(@book1)
    @series3.add_book(@book6)
    @series2.add_book(@book6)
    @series1.add_book(@book6)

    
    @book7 = Book.create(:title => "Tom Sawyer")
    @book8 = Book.create(:title => "Huckleberry Finn")
    ChangeTracker.commit
    
    # Get the join entry for series3 => book4
    join = @series3.books_records[0]
    join.series.id.should == @series3.id
    join.book.id.should == @book4.id
    join.series_position.should == 0
    join.book_position.should == 1
    @series3.books.count.should == 3
    @book4.series.count.should == 2
    # Set the book to book7
    ChangeTracker.start
    join.book = @book7
    ChangeTracker.commit
    # Check associations
    join.series.id.should == @series3.id
    join.book.id.should == @book7.id
    # Position in books should not have changed (took the place of another book)
    join.series_position.should == 0
    # Series position is now 0 (Only a member of 1 series)
    join.book_position.should == 0
    
    
    # Get the join entry for series2 => book5
    join = @series2.books_records[1]
    join.series_position.should == 1
    join.book_position.should == 0
    # Set the book to book8
    ChangeTracker.start
    join.book = @book8
    ChangeTracker.commit
    # Check positions
    join.series_position.should == 1
    join.book_position.should == 0
    
    # Check number of associations
    @series3.books.count.should == 3
    @book4.series.count.should == 1
    @book7.series.count.should == 1
    
    # Set association by creating new instance of association class
    # Using series2 => book7
    @series2.books.count.should == 3
    @book7.series.count.should == 1
    ChangeTracker.start
    join = BooksInSeries.create
    ChangeTracker.commit
    ChangeTracker.start
    join.book = @book7
    ChangeTracker.commit
    join.book_position.should == 1
    ChangeTracker.start
    join.series = @series2
    ChangeTracker.commit
    join.series_position.should == 3
    
    @series2.books.count.should == 4
    @series2.books[3].id.should == @book7.id
    @series2.books_records[3].series_position.should == 3
    @book7.series.count.should == 2
    @book7.series[1].id.should == @series2.id
    @book7.series_records[1].book_position.should == 1
  end
  
  # TODO: move this test to a more appropriate location
  it "should update the reference to an object when performing operations via hooks" do

    ChangeTracker.start
    @series2.add_book(@book4)
    @series2.add_book(@book5)
    @series2.save
    ChangeTracker.commit
    series2_one = Series[@series2.id]
    @series2.books.count.should == 2
    @series2.books[0].id.should == @book4.id
    @series2.books[1].id.should == @book5.id
    # For some reason, the modification of @series3 here affects this test, so leaving this in.
    ChangeTracker.start
    @series3.add_book(@book4)
    @series3.add_book(@book1)
    @series3.add_book(@book6)
    ChangeTracker.commit
    series2_two = Series[@series2.id]
    
    ChangeTracker.start
    series2_three = Series[@series2.id]
    @series2.should == series2_one
    @series2.should == series2_two
    @series2.should == series2_three
    @series2.add_book(@book6)
    
    ChangeTracker.commit
    @series2.books.count.should == 3
    @series2.books[0].id.should == @book4.id
    @series2.books[1].id.should == @book5.id
    @series2.books[2].id.should == @book6.id
  end
  
  # TODO: move this test to a more appropriate location
  it "should update the ct_version of an object when refresh is called" do
    old_series_2 = Series[@series2.id]
    ChangeTracker.start
    @series2.add_book(@book4)
    @series2.add_book(@book5)
    @series2.save
    ChangeTracker.commit
    
    @series2.books.count.should == 2
    @series2.books[0].id.should == @book4.id
    @series2.books[1].id.should == @book5.id
    
    ChangeTracker.start
    old_series_2.refresh
    old_series_2.add_book(@book6)
    # Would get basis changed error here unless refresh updates ct_version
    lambda {ChangeTracker.commit}.should_not raise_error
  end
end

# TODO: time-browsing should work with ordered associations

# TODO: association class methods should work with ordered associations (important)