require File.dirname(__FILE__) + '/spec_helper.rb'

describe ChangeTracker, "#reset_all" do
  before(:each) do
    ChangeTracker.cancel
    clear_db
  end
  after(:each) do
    setup_session
  end
  
  it "should reset all sessions" do
    ChangeTracker.author.should == 'username'
    ChangeTracker.start
    ChangeTracker.reset_all
    ChangeTracker.started?.should be false
    # NOTE: username is not reset, since it is read from a source external to SCT's session
    ChangeTracker.author.should == 'username'
  end
end

describe ChangeTracker, "#commit" do
  before(:each) do
    ChangeTracker.cancel
    clear_db
  end
  
  it "should save changes to db on commit" do
    #puts ChangeTracker.db.inspect
    ChangeTracker.start
    a = Actor.create(:name => 'test actor')
    ChangeTracker.commit.should be true
    Actor[:name => 'test actor'].should_not be_nil
  end
  
  it "should not save any changes without a commit" do
    ChangeTracker.start
    a = Actor.create(:name => 'test actor')
    Actor.all.should be_empty
  end
  
  it "should give an error, on object manipulation and on commit if no unit of work has been started" do
    lambda{ a = Actor.create(:name => 'test actor') }.should raise_error ChangeTracker::NotStartedError
    lambda{ ChangeTracker.commit }.should raise_error ChangeTracker::NotStartedError
    Actor.all.should be_empty
  end
  
  it "should create and persist change objects and a unit of work object" do
    ChangeTracker.start
    a = Actor.new(:name => 'test actor')
    a.save
    ChangeTracker.commit
    actor = a.refresh
    actor.should_not be_nil
    # Only a ChangeLifecycle is recorded, since the ChangeAttribute can be inferred
    actor.ct_history.changes.length.should == 1
  end
  
  it "should allow a block to be passed" do
    ChangeTracker.commit {
      a = Actor.new(:name => 'test actor')
      a.save
    }
    ChangeTracker.started?.should be false
    actor = Actor.first
    actor.should_not be_nil
    actor.name.should == 'test actor'
  end
end

# These are not complete. They include only
# Change_Lifecycle_Semantics :Create
# Change_Collection_Semantics :Insert
# I think these examples are good enough that it should be clear how to add the others.

# Presumes setup code creates a fresh subclass of Domain_Object, which is returned by obj
describe DomainObject, " when created, persisted, and retrieved" do
  before(:each) do
    clear_db
    ChangeTracker.start
    a = Actor.create
    ChangeTracker.commit
    ChangeTracker.start
    @obj = Actor[a.id]
  end
  
  it "has an Identity" do
    @obj.ct_identity.should_not be_nil
    @obj.ct_identity.key.should be_an_instance_of(Integer) # Key used in RDB
    @obj.ct_identity.class_name.should be_an_instance_of(String)
  end
  it "has a History that contains one change, which is a Create Change_Lifecycle" do
    @obj.ct_history.size.should == 1
    @obj.ct_history[0].should be_an_instance_of(ChangeTracker::ChangeLifecycle)
    @obj.ct_history[0].semantics.should == "Create"
  end
  it "ChangeLifecycle should not have any problems" do
    @obj.ct_history[0].problems.should be_empty
  end
  it "ChangeLifecycle knows the UnitOfWork it was created under" do
    change = @obj.ct_history[0]
    uow = change.unit_of_work
    uow.should be_an_instance_of(ChangeTracker::UnitOfWork)
    uow.basis_readtime.should be_a_kind_of(Time) # When the view that the commit is based on was taken
    uow.submission_time.should be_a_kind_of(Time) # When the unit of work was submitted
    uow.completion_time.should be_a_kind_of(Time) # When the commit was fishished
    uow.basis_readtime.should < uow.submission_time
    uow.submission_time.should < uow.completion_time
    uow.author.should be_an_instance_of(String)
    uow.author.should == 'username'
  end
  it "should support changing the author key" do
    ChangeTracker.user_session_hash[:full_name] = "User Name"
    ChangeTracker.author.should == 'username'
    ChangeTracker.unit_of_work.author.should == 'username'
    # Change author key and retest
    ChangeTracker.author_key = :full_name
    ChangeTracker.author.should == 'User Name'
    # This shouldn't change since it is a value stored previously
    ChangeTracker.unit_of_work.author.should == 'username'
    
    # Set the author_key back to default
    ChangeTracker.author_key = nil
    ChangeTracker.author.should == 'username'
    ChangeTracker.unit_of_work.author.should == 'username'
  end
end


describe DomainObject, " when creating an association" do
  before(:each) do
    clear_db
    ChangeTracker.start
    a = Actor.create
    c = Constrained.create
    @uow0 = ChangeTracker.unit_of_work
    ChangeTracker.commit
    ChangeTracker.start
    @actor = Actor[a.id]
    @constrained = Constrained[c.id]
  end
  
  it "should interact with SSA to create a ChangeAssociation change for an association" do
    @actor.add_constrained_goal(@constrained)
    # Association change to associate actor with constrained
    ChangeTracker.unit_of_work.changes.last.should be_an_instance_of(ChangeTracker::ChangeAssociation)
    ChangeTracker.unit_of_work.changes.size.should == 1
    ChangeTracker.commit
    @actor.constrained_goal.should include(@constrained)
    @constrained.actor_goal.should include(@actor)
  end

  it "should show uncommited associations when using the ORM methods" do
    @actor.constrained_goal.should be_empty
    @actor.add_constrained_goal(@constrained)
    @actor.constrained_goal.should_not be_empty
    ChangeTracker.commit
    @actor.constrained_goal.should_not be_empty
    ChangeTracker.start
    @actor.remove_constrained_goal(@constrained)
    @actor.constrained_goal.should be_empty
    ChangeTracker.commit
    @actor.constrained_goal.should be_empty
  end
  
  it "should convert the ChangeAssociation into ChangeAttributes that are also stored on commit" do
    @actor.add_constrained_goal(@constrained)
    # Association change to associate actor with constrained
    ChangeTracker.unit_of_work.changes.last.should be_an_instance_of(ChangeTracker::ChangeAssociation)
    ChangeTracker.unit_of_work.changes.size.should == 1
    uow = ChangeTracker.unit_of_work
    ChangeTracker.commit
    @actor.constrained_goal.should include(@constrained)
    @constrained.actor_goal.should include(@actor)
    # Retrieve join entry
    @goal = @actor.constrained_goal_associations.first[:through]
    
    # Check changes stored in the UoW
    unit_of_work = ChangeTracker::UnitOfWork[uow.id]
    unit_of_work.should be_an_instance_of(ChangeTracker::UnitOfWork)
    saved_changes = unit_of_work.changes
    # Change Association should generate 1 ChangeLifecycle change and 4 ChangeAttribute changes
    saved_changes[0].should be_an_instance_of(ChangeTracker::ChangeLifecycle)
    # Change Association should be persisted
    saved_changes[1].should be_an_instance_of(ChangeTracker::ChangeAssociation)
    saved_changes.length.should == 2
    # Test changes' stored positions
    saved_changes[0].position.should == 0
    saved_changes[1].position.should == 1
    
    # The ChangeAssociation should be listed as a change for both ends of the association, as well as the join entry
    @actor.ct_history.changes.last.should be_an_instance_of(ChangeTracker::ChangeAssociation)
    @constrained.ct_history.changes.last.should be_an_instance_of(ChangeTracker::ChangeAssociation)
    @goal.ct_history.changes.last.should be_an_instance_of(ChangeTracker::ChangeAssociation)
  end
  
  it "should be able to undo association changes for a many-to-many" do
    @constrained2 = Constrained.create
    # Add first constrained
    @actor.add_constrained_goal(@constrained)
    uow1 = ChangeTracker.unit_of_work
    ChangeTracker.commit
    # Add second constrained
    ChangeTracker.start
    @actor.add_constrained_goal(@constrained2)
    uow2 = ChangeTracker.unit_of_work
    ChangeTracker.commit
    # Remove all constrained relationships
    ChangeTracker.start
    @actor.remove_all_constrained_goal
    uow3 = ChangeTracker.unit_of_work
    ChangeTracker.commit
    # Test history of association for @actor
    # Test current time
    @actor.constrained_goal.should =~ []
    @actor.ct_history.version_at(Time.now).constrained_goal.should =~ []
    # Test rewinding past uow3
    # NOTE: Currently SCT doesn't rewind associations for elements that do not hold the association key.
    #       In order to support this, SCT would have to set some flag on the object that is checked by SSA when
    #       executing association methods (e.g. constrained_goals here would have to know that this is a historical
    #       version of the object).
    @actor.ct_history.version_at(uow2.completion_time).constrained_goal.should =~ [] # Should be [@constrained, @constrained2]
    # Test rewinding past uow2
    @actor.ct_history.version_at(uow1.completion_time).constrained_goal.should =~ [] # Should be [@constrained]
    # test rewinding past uow1
    @actor.ct_history.version_at(@uow0.completion_time).constrained_goal.should =~ []
    
    # Test history of association for @constrained
    @constrained.actor_goal.should =~ []
    @constrained.ct_history.version_at(Time.now).actor_goal.should =~ []
    # Test rewinding past uow3
    @constrained.ct_history.version_at(uow2.completion_time).actor_goal.should =~ []
    # Test rewinding past uow2
    @constrained.ct_history.version_at(uow1.completion_time).actor_goal.should =~ [] # Should be [@actor]
    # Test rewinding past uow1
    @constrained.ct_history.version_at(@uow0.completion_time).actor_goal.should =~ []
    
    # Test history of association for @constrained2
    @constrained2.actor_goal.should =~ []
    @constrained2.ct_history.version_at(Time.now).actor_goal.should =~ []
    # Test rewinding past uow3
    @constrained2.ct_history.version_at(uow2.completion_time).actor_goal.should =~ [] # Should be [@actor]
    # Test rewinding past uow2
    @constrained2.ct_history.version_at(uow1.completion_time).actor_goal.should =~ []
    # Test rewinding past uow1 (@constrained2 has not yet been created)
    @constrained2.ct_history.version_at(@uow0.completion_time).should == nil
  end
  
  it "should be able to undo association changes for a many-to-one" do
    # Create initial objects
    person = People::Person.create
    mechanic = Automotive::Mechanic.create
    car = Automotive::Car.create
    uow1 = ChangeTracker.unit_of_work
    ChangeTracker.commit
    # Add first occupant to car
    ChangeTracker.start
    car.add_occupant(person)
    uow2 = ChangeTracker.unit_of_work
    ChangeTracker.commit
    # Add second occupant to car
    ChangeTracker.start
    car.add_occupant(mechanic)
    uow3 = ChangeTracker.unit_of_work
    ChangeTracker.commit
    # Remove all occupants from car
    ChangeTracker.start
    car.remove_all_occupants
    uow4 = ChangeTracker.unit_of_work
    ChangeTracker.commit
    # Refresh person and mechanic to reflect removal
    person.refresh
    mechanic.refresh
    
    # Test history of occupants association for car
    # Test current time
    car.occupants.should =~ []
    car.ct_history.version_at(Time.now).occupants.should =~ []
    # Test rewinding past uow4
    # NOTE: Currently SCT doesn't rewind associations for elements that do not hold the association key.
    #       In order to support this, SCT would have to set some flag on the object that is checked by SSA when
    #       executing association methods (e.g. constrained_goals here would have to know that this is a historical
    #       version of the object).
    car.ct_history.version_at(uow3.completion_time).occupants.should =~ [] # Should be [person, mechanic]
    # Test rewinding past uow3
    car.ct_history.version_at(uow2.completion_time).occupants.should =~ [] # Should be [person]
    # test rewinding past uow2
    car.ct_history.version_at(uow1.completion_time).occupants.should =~ []
    
    # Test history of occupying association for person
    person.occupying.should == nil
    person.ct_history.version_at(Time.now).occupying.should == nil
    # Test rewinding past uow4
    person.ct_history.version_at(uow3.completion_time).occupying.should == car
    # Test rewinding past uow3
    person.ct_history.version_at(uow2.completion_time).occupying.should == car
    # Test rewinding past uow2
    person.ct_history.version_at(uow1.completion_time).occupying.should == nil
    
    # Test history of occupying association for mechanic
    mechanic.occupying.should == nil
    mechanic.ct_history.version_at(Time.now).occupying.should == nil
    # Test rewinding past uow4
    mechanic.ct_history.version_at(uow3.completion_time).occupying.should == car
    # Test rewinding past uow3
    mechanic.ct_history.version_at(uow2.completion_time).occupying.should == nil
    # Test rewinding past uow2
    mechanic.ct_history.version_at(uow1.completion_time).occupying.should == nil
  end
  
  it "should interact with SSA to create a ChangeAssociation change for an association for an as yet uncreated object" do
    @actor.add_constrained_goal(Constrained.create)
    # Lifecycle change to create constrained
    ChangeTracker.unit_of_work.changes[0].should be_an_instance_of(ChangeTracker::ChangeLifecycle)
    # Association change to associate actor with new constrained
    ChangeTracker.unit_of_work.changes[1].should be_an_instance_of(ChangeTracker::ChangeAssociation)
    ChangeTracker.unit_of_work.changes.size.should == 2
    ChangeTracker.commit
    Constrained.all.length.should == 2
    constrained = Constrained.all.last
    @actor.constrained_goal.should include(constrained)
    constrained.actor_goal.should include(@actor)
  end
  
  it "Should automatically create ChangeLifecycle changes if an association is created from/to unsaved objects" do
    a1 = Actor.new
    a2 = Actor.new
    c1 = Constrained.new
    c2 = Constrained.new
    
    a1.add_constrained_goal(c1)
    a2.add_constrained_goal(c2)
    ChangeTracker.commit
    a1.constrained_goal.should =~ [c1]
    c1.actor_goal.should =~ [a1]
    a2.constrained_goal.should =~ [c2]
    c2.actor_goal.should =~ [a2]
  end
  
  # This test was added because association reordering in gui_site relies on this feature. (See "def parse_ordering_change")
  # If that is no longer the case, this test may be removed. -SD
  it "should create an AttributeChange for a new object with a modified attribute" do
    a1 = Actor.new
    a1[:description_id] = 101
    changes = a1.check_for_attribute_changes
    changes.should be_a(Array)
    changes.length.should == 1
    changes[0].should be_a(ChangeTracker::ChangeAttribute)
  end
end


# Presumes obj has an accessor for an integer value named "setting" which defaults to 7
DB.create_table? :simple_objects do
  primary_key :id
  Fixnum :setting, :default => 7 #looks like default is ignored as of Sequel v3.31.0...
end
class SimpleObject < Sequel::Model
  plugin :change_tracker
  
end
describe DomainObject, " when simple attribute is modified" do
  before(:each) do
    # Turn SSA's enforcement of abstract classes back on (turned off for last test)
    clear_db
    ChangeTracker.start
    # Setup the object
    s = SimpleObject.create(:setting => 7)
    ChangeTracker.commit
    ChangeTracker.start
    @obj = SimpleObject[s.id]
    # End object setup
  end

  it "history gets a new ChangeAttribute" do
    @obj.setting.should == 7
    initial_size = @obj.ct_history.size
    @obj.setting=9
    
    @obj.save
    ChangeTracker.commit
    ChangeTracker.start
    
    @obj.setting.should == 9
    @obj.ct_history.size.should == 1+initial_size
    change = @obj.ct_history.last
    change.should be_an_instance_of(ChangeTracker::ChangeAttribute)
    change.old_value.to_i.should == 7
    change.new_value.to_i.should == 9
    # replace
    time_before_change = Time.now # before the next change
    @obj.setting=3
    @obj.setting.should == 3
    
    @obj.save
    ChangeTracker.commit
    ChangeTracker.start
    @obj.ct_history.size.should == 2+initial_size
    change = @obj.ct_history.last
    change.should be_an_instance_of(ChangeTracker::ChangeAttribute)
    change.old_value.to_i.should == 9
    change.new_value.to_i.should == 3
    # history knows changes after a given time
    changes = @obj.ct_history.changes_since(time_before_change)
    changes.length.should == 1
    changes[0].should be_an_instance_of(ChangeTracker::ChangeAttribute)
    changes.should == [change]
    # history can reconstruct object before change
    old = @obj.ct_history.version_at(time_before_change)
    old.should be_an_instance_of(@obj.class)
    old.setting.should == 9 # we changed it twice to avoid testing against the default value
  end
end


describe DomainObject, " when a frozen attribute is saved" do
  before(:each) do
    clear_db
    true.freeze # Permanently freezes true!
    ChangeTracker.start
  end

  it "should save a frozen boolean attribute" do
    user = User.create(:active => true)
    ChangeTracker.commit
    User[user.id].active.should == true
  end
  
  it "should save a frozen string attribute" do
    actor = Actor.create(:name => 'Bob'.freeze)
    ChangeTracker.commit
    Actor[actor.id].name.should == 'Bob'
  end
end

describe DomainObject, " when a time attribute is modified" do
  before(:each) do
    clear_db
    ChangeTracker.start
    # Setup the object
    #s = Sign_Off.create
    ChangeTracker.commit
    ChangeTracker.start
    #@obj = Sign_Off[s.id]
    # End object setup
  end

  it "history gets a new ChangeAttribute" do
    @obj = Sign_Off.create
    @obj._when.should be_nil
    t = Time.now
    @obj._when = t
    @obj.save
    ChangeTracker.commit
    s = Sign_Off[@obj.id]
    s._when.to_i.should be_within(1).of(t.to_i)
    
  end
  
  it "should work with a time string" do
    @obj = Sign_Off.new
    @obj._when.should be_nil
    t = "09/04/2010"
    @obj._when = t
    @obj.save
    ChangeTracker.commit
    s = Sign_Off[@obj.id]
    s._when.to_i.should be_within(1).of(Time.parse(t).to_i)
  end

end

describe DomainObject, " when there is a problem" do
  before(:each) do
    clear_db
    ChangeTracker.start
    a = Actor.create(:name => 'actor_a', :actor_super_class => 'testing')
    ChangeTracker.commit
    ChangeTracker.start
  end
  
  it "should create a problem object and stack dump and attach it to the change for problems" do
    a = Actor[:name => 'actor_a']
    a.destroy
    ChangeTracker.commit.should == true
    
    
    # Functionality Changed. Now only warns for deleting already deleted item.
    #ChangeTracker.start
    #a.destroy # Should cause error
    #lambda{uow = ChangeTracker.commit}.should raise_error(/Cannot destroy an already deleted/i)
    
    
    # uow.should be_an_instance_of(ChangeTracker::UnitOfWork)
    # bad_change = uow.changes.last
    # bad_change.problems.length.should == 1
    # problem = bad_change.problems[0]
    # problem.description.should match(/Cannot destroy an already deleted item/i)
    # problem.stack_dump.should match("lib/sequel_change_tracker/model/change.rb:")
    
    ChangeTracker.start
    test = Actor.create
    ChangeTracker.commit
    test.id.should_not == a.id
    test.id.should > a.id
    
    # Make sure that the uow did not affect the DB (it should be rolled back on errors)
    Actor[:name => 'actor_a'].should be_nil
    Actor.deleted[:name => 'actor_a'].should_not be_nil
    a_del = Actor.deleted[:name => 'actor_a']
    a_del[:actor_super_class].should == 'testing'
    a_del.id.should == a.id
  end
end

describe DomainObject, " when multiple change types are made" do
  before(:each) do
    clear_db
    ChangeTracker.start
    a = Actor.create(:name => 'actor_a', :actor_super_class => 'testing')
    b = Actor.new(:name => 'actor_b')
    b.save
    ChangeTracker.commit
    ChangeTracker.start
    b[:actor_super_class] = 'testing'
    b.save
    ChangeTracker.commit
    ChangeTracker.start
  end
  
  it "should correctly persist all changes" do
    test_a = Actor[:name => 'actor_a']
    test_b = Actor[:name => 'actor_b']
    # Only a ChangeLifecycle is recorded, since the ChangeAttribute can be inferred
    test_a.ct_history.changes.length.should == 1
    # A ChangeLifecycle is recorded, followed by a separate ChangeAttribute (it occurred in a later UOW)
    test_b.ct_history.changes.length.should == 2
  end
  
  it "should allow access to a query returning all changes to an object" do
    b = Actor[:name => 'actor_b']
    h = b.ct_history
    changes = h.change_query.all
    changes.should be_an Array
    first_change = changes.first
    first_change.should be_a ChangeTracker::ChangeLifecycle
    first_change.semantics.should == 'Create'
    # The change query can also return an array of hashes instead of objects for efficiency
    change_hashes = h.change_query(use_model=false).all
    change_hashes.should be_an Array
    first_change_hash = change_hashes.first
    first_change_hash.should be_a Hash
    first_change_hash[:semantics].should == 'Create'
    # Hash also includes author and completion time attributes from corresponding UOW
    first_change_hash[:author].should == 'username'
    first_change_hash[:completion_time].should be_a Time
  end
  
  it "should perform reconstructions of old versions" do
    b = Actor[:name => 'actor_b']
    h = b.ct_history
    v = h.versions
    
    v.length.should == 2
    
    old_b = h.version_at(v[0] - 1)
    old_b.should be_nil
    
    old_b = h.version_at(v[0])
    old_b.name.should == "actor_b"
    old_b[:actor_super_class].should be_nil
    
    old_b = h.version_at(v[1]) #current version
    old_b.name.should == "actor_b"
    old_b[:actor_super_class].should == 'testing'
    
    # Should not affect current object
    old_b = h.version_at(v[0])
    b[:actor_super_class].should_not be_nil
  end
  
  it "should be able to return all relevant units of work for an object" do
    b = Actor[:name => 'actor_b']
    h = b.ct_history
    
    uows = h.units_of_work
    uows.length.should == 2
    uows.first.completion_time.should < uows.last.completion_time
  end
  
  it "should hide deleted items from normal access" do
    a = Actor[:name => 'actor_a']
    a.destroy
    ChangeTracker.commit
    Actor[:name => 'actor_a'].should be_nil
    actor_names = Actor.all.collect {|actor| actor.name }
    actor_names.should_not include("actor_a")
  end
  
  it "should allow access to deleted items through the deleted dataset" do
    a = Actor[:name => 'actor_a']
    b = Actor[:name => 'actor_b']
    b.destroy
    ChangeTracker.commit
    Actor.all.length.should == 1
    Actor[:name => 'actor_a'].should_not be_nil
    Actor[:name => 'actor_b'].should be_nil
    
    # Now test .deleted dataset
    Actor.deleted.all.length.should == 1
    Actor.deleted[:name => 'actor_a'].should be_nil
    Actor.deleted[:name => 'actor_b'].should_not be_nil
    Actor.deleted.first.values[:name].should == 'actor_b'
  end
  
  it "should allow re-creation of deleted objects" do
    initial_num_actors = Actor.all.length
    b = Actor[:name => 'actor_b']
    time_before_delete = Time.now
    b.destroy
    ChangeTracker.commit
    Actor[:name => 'actor_b'].should be_nil
    Actor.all.length.should == initial_num_actors - 1
    
    b_del = Actor.deleted[:name => 'actor_b']
    b_del.should_not be_nil
    
    h = b_del.ct_history
    h.version_at(Time.now).should be_nil
    h.changes_since(time_before_delete).length.should == 1
    old_b = h.version_at(time_before_delete)
    old_b.should_not be_nil
    old_b.name.should == 'actor_b'
    
    ChangeTracker.start
    old_b.save
    # puts ChangeTracker.unit_of_work.changes.inspect
    ChangeTracker.commit

    Actor.all.length.should == initial_num_actors
    new_b = Actor[:name => 'actor_b']
    new_b.should_not be_nil
    
    # ID's should stay the same
    b.id.should == old_b.id
    old_b.id.should == new_b.id
  end
  
  it "should not reassign ids of deleted items" do
    test_a = Actor[:name => 'actor_a']
    test_b = Actor[:name => 'actor_b']
    test_a.destroy
    test_b.destroy
    ChangeTracker.commit
    ChangeTracker.start
    
    a_id = Actor.deleted[:name => 'actor_a'].id
    b_id = Actor.deleted[:name => 'actor_b'].id
    new1 = Actor.create
    new2 = Actor.create
    new3 = Actor.create
    ChangeTracker.commit
    ChangeTracker.start
    new1.id.should > a_id
    new1.id.should > a_id
    new2.id.should > a_id
    new3.id.should > a_id
    new1.id.should > b_id
    new2.id.should > b_id
    new3.id.should > b_id
    # Sanity check
    Actor.all.length.should == 3
    Actor.deleted.all.length.should == 2
  end
end

describe ChangeTracker, " when many changes are present" do
  before(:each) do
    clear_db
    10.times do |i|
      ChangeTracker.start
      actors = []
      10.times do |j|
        a = Actor.create(:name => "actor_#{i}_#{j}", :actor_super_class => 'a')
        a[:actor_super_class] = 'b'
        a.save
        actors << a
      end
      # Set the first actor's 'actor_sub' association to the rest of the created actors
      actors.first.actor_sub = actors[1..-1]
      ChangeTracker.commit
    end
    
    @actor = Actor.all.last
    10.times do |i|
      ChangeTracker.start
      100.times do |j|
        @actor[:actor_super_class] = j.to_s
        @actor.save
      end
      ChangeTracker.commit
    end
  end
  
  it "should return a list of changes and units of work related to a history" do
    t = Time.now
    changes = @actor.ct_history.change_query(false).all
    change_query_time = Time.now - t
    # puts "Took: #{change_query_time} to get #{changes.count} Changes"
    
    t = Time.now
    uows = @actor.ct_history.uow_query(false).all
    uow_query_time = Time.now - t
    # puts "Took: #{uow_query_time} to get #{uows.count} UOWs"
    
    # Performance of uow query should be better than change query
    uow_query_time.should < change_query_time
  end
end

describe ChangeTracker, "when setting a browse time" do
  before :each do
    ChangeTracker.browse_time = nil
    clear_db
    @time_before_creation = Time.now
    ChangeTracker.start
    @actor = Actor.create(:name => 'myActor')
    @system = System.create(:name => 'mySystem')
    ChangeTracker.commit
    @time_after_creation = Time.now
    ChangeTracker.start
    @actor.name = 'newActor'
    @system.name = 'newSystem'
    @actor.save
    @system.save
    ChangeTracker.commit
    @time_after_update = Time.now
  end
  
  it "should only return objects as they existed at the browse time" do
    # A ChangeLifecycle is recorded, followed by a separate ChangeAttribute (it occurred in a later UOW)
    Actor[@actor.id].ct_history.changes_since(@time_before_creation).length.should == 2
    ChangeTracker.browse_time = @time_after_creation
    a = Actor[@actor.id]
    a.name.should == 'myActor'
    s = System[@system.id]
    s.name.should == 'mySystem'
    ChangeTracker.browse_time = @time_after_update
    a = Actor[@actor.id]
    a.name.should == 'newActor'
    s = System[@system.id]
    s.name.should == 'newSystem'
    ChangeTracker.browse_time = @time_before_creation
    a = Actor[@actor.id]
    a.should be_nil
    s = System[@system.id]
    s.should be_nil
    # Removing browse time should return to most recent time
    ChangeTracker.browse_time = nil
    Actor[@actor.id].name.should == 'newActor'
    System[@system.id].name.should == 'newSystem'
  end
  
  it "should successfully retrieve items that have been deleted" do
    @time_before_delete = Time.now
    ChangeTracker.start
    @actor.destroy
    @system.destroy
    ChangeTracker.commit
    @time_after_delete = Time.now
    Actor[@actor.id].should be_nil
    System[@system.id].should be_nil
    # Set the browse time
    ChangeTracker.browse_time = @time_before_delete
    Actor[@actor.id].should_not be_nil
    System[@system.id].should_not be_nil
    Actor[@actor.id].name.should == 'newActor'
    System[@system.id].name.should == 'newSystem'
    # Remove the browse time and test that the objects are still deleted
    ChangeTracker.browse_time = nil
    Actor[@actor.id].should be_nil
    System[@system.id].should be_nil
    # Set the browse time to after the deletion and test that the objects are deleted
    ChangeTracker.browse_time = @time_after_delete
    Actor[@actor.id].should be_nil
    System[@system.id].should be_nil
    ChangeTracker.browse_time = nil    
  end
  
  it "should return a correct listing of all items of a model for the given time" do
    @time_before_delete = Time.now
    ChangeTracker.start
    @actor.destroy
    ChangeTracker.commit
    @time_after_delete = Time.now
    Actor.all.length.should == 1
    System.all.length.should == 1
    System.all[0].should be_an_instance_of(System)
    ChangeTracker.browse_time = @time_before_delete
    Actor.all.length.should == 2
    Actor.all[1].should be_an_instance_of(Actor)
    System.all.length.should == 1
    System.all[0].should be_an_instance_of(System)
    ChangeTracker.browse_time = @time_after_delete
    Actor.all.length.should == 1
    System.all.length.should == 1
    System.all[0].should be_an_instance_of(System)
    ChangeTracker.browse_time = nil
    Actor.all.length.should == 1
    System.all.length.should == 1
    System.all[0].should be_an_instance_of(System)
    ChangeTracker.browse_time = nil
  end
end

describe ChangeTracker, "when using limited mode" do
  before :each do
    ChangeTracker.browse_time = nil
    clear_db
  end
  
  after :all do
    ChangeTracker.extra_limited_mode = false
    ChangeTracker.limited_mode = false
  end
  
  it "should only save ChangeLifecycle changes when using limited mode" do
    ChangeTracker.limited_mode = true
    ChangeTracker.start
    p = People::Person.create
    c = Automotive::Car.create
    ChangeTracker.commit
    ChangeTracker.start
    p.name = 'Bob'
    c.add_occupant(p)
    ChangeTracker.commit
    # Test that changes were successful
    p.name.should == 'Bob'
    # If this check fails, there may be a problem with Time precision
    c.occupants.first.updated_at.should == p.updated_at
    c.occupants.should == [p]
    p.occupying.should == c
    # Test that only ChangeLifecycle changes were persisted
    ChangeTracker::Change.count.should == 2
    ChangeTracker::Change.all.each{|c| c.should be_an_instance_of(ChangeTracker::ChangeLifecycle)}
    # Test that units of work were saved
    ChangeTracker::UnitOfWork.count.should == 2
    uow1, uow2 = ChangeTracker::UnitOfWork.all
    uow1.changes.count.should == 2
    uow2.changes.count.should == 0
  end
  
  it "should save no changes when using extra limited mode" do
    ChangeTracker.extra_limited_mode = true
    ChangeTracker.start
    p = People::Person.create
    c = Automotive::Car.create
    ChangeTracker.commit
    ChangeTracker.start
    p.name = 'Bob'
    c.add_occupant(p)
    ChangeTracker.commit
    # Test that changes were successful
    p.name.should == 'Bob'
    c.occupants.should == [p]
    p.occupying.should == c
    # Test that no changes were persisted
    ChangeTracker::Change.count.should == 0
    # Test that units of work were not saved
    ChangeTracker::UnitOfWork.count.should == 0
  end
end

# Presumes obj has an accessor for "pet" which defaults to nil, but can be a Cat or Dog

# class Person < Sequel::Model
#   plugin :change_tracker
#   
# end

 
# describe My_Domain_Object_Subclass, " when associated object is changed" do
#   before(:each) do
#     ChangeTracker.commit
#     clear_db
#     ChangeTracker.start
#     a = Person.create
#     ChangeTracker.commit
#     ChangeTracker.start
#     @obj = Person[a.id]
#   end
#   
#   it "the history gets a new Change_Attribute" do
#     @obj.pet.should be_nil
#     initial_size = obj.ct_history.size
#     @fido = Cat.new('Fido')
#     obj.pet= @fido
#     obj.pet.should == @fido
#     obj.ct_history.size.should == 1+initial_size
#     change = obj.ct_history.last
#     change.should be_an_instance_of(ChangeAttribute)
#     change.old_value.should be_nil
#     change.new_value.should == @fido.identity
#     # replace
#     # I've not tried persisting state between specs. Might not work
#     @time_before_change = Time.now # before the next change
#     @fluffy = Dog.new('Fluffy')
#     obj.pet=@fluffy
#     obj.pet.should ==@fluffy
#     obj.ct_history.size.should == 2+initial_size
#     @change = obj.ct_history.last
#     @change.should be_an_instance_of(ChangeAttribute)
#     @change.old_value.should == @fido.identity
#     @change.new_value.should == @fluffy.identity
#   end
#   it "ct_history knows changes after a given time" do
#     changes = obj.ct_history.changes_since(@time_before_change)
#     changes.should == [@change]
#   end
#   it "ct_history can reconstruct object before change" do
#     old = obj.ct_history.version_at(@time_before_change)
#     old.should be_an_instance_of(obj.class)
#     old.pet.should == @fido # we changed it twice to avoid testing against the default value
#   end
# end
# 
# 
# 
# # Presumes obj has an accessor for "colors" which is an initially empty array of Color instances
# describe My_Domain_Object_Subclass, " when a collection is modified" do
#   it "the ct_history gets a new Change_Attribute" do
#     obj.colors.should be_empty
#     initial_size = obj.ct_history.size
#     @red = Color.new('Red')
#     obj.add_color @red
#     obj.colors.last.should == @red
#     obj.ct_history.size.should == 1+initial_size
#     change = obj.ct_history.last
#     change.should be_an_instance_of(ChangeCollection)
#     change.semantics.should == :Insert
#     change.old_value.should be_nil
#     change.new_value.should == @red.identity
#     change.key.should == 0
#     change.new_key.should be_nil
#     # another
#     # I've not tried persisting state between specs. Might not work
#     @time_before_change = Time.now # before the next change
#     @blue = Color.new('Blue')
#     obj.add_color @blue
#     obj.colors.last.should == @blue
#     obj.ct_history.size.should == 2+initial_size
#     @change = obj.ct_history.last
#     @change.should be_an_instance_of(ChangeCollection)
#     @change.semantics.should == :Insert
#     @change.old_value.should == nil
#     @change.new_value.should == @blue.identity
#     @change.key.should == 1
#     @change.new_key.should be_nil
#   end
#   it "ct_history knows changes after a given time" do
#     changes = obj.ct_history.changes_since(@time_before_change)
#     changes.should == [@change]
#   end
#   it "ct_history can reconstruct object before change" do
#     old = obj.ct_history.version_at(@time_before_change)
#     old.should be_an_instance_of(obj.class)
#     old.colors.last.should == @red # we changed it twice to avoid testing against the default value
#   end
# end
