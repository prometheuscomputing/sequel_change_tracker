# class Sequel::Model
#   class << self
#     attr_accessor :immediate_children
#     # alias sequel's inherited method, then call it from new inherited method
#     alias_method :inherited_original, :inherited
#     # When a child class is created, add it to the immediate children array
#     def inherited(subclass)
#       inherited_original(subclass)
#       @immediate_children ||= Array.new
#       @immediate_children << subclass
#     end
#     # Return all children of this class
#     def child_classes
#       all_children = Array.new
#       if @immediate_children
#         all_children += @immediate_children
#         @immediate_children.each do |child_class|
#           all_children += child_class.child_classes
#         end
#         all_children
#       else
#         Array.new
#       end
#     end
#     def abstract!
#       @is_abstract = true
#     end
#     def abstract?
#       @is_abstract == true
#     end
#     def association_class!
#       @association_class = true
#     end
#     def association_class?
#       @association_class == true
#     end
#   end # End class << self
# end # End Sequel::Model

module SequelDateTimePatch
  # typecast_value_datetime does not like a java.util.Date object, so we need
  # to convert it before we get there.  DB should extend this module so that it
  # goes here before it reaches Datebase.typecast_value_datetime
  def typecast_value_datetime(value)
    if value.kind_of?(java::util::Date)
      super DateTime.parse(value.to_s)
    else
      super
    end
  end
end

module SequelDatasetPatch
  # Split out from fetch rows to allow processing of JDBC result sets
  # that don't come from issuing an SQL string.
  def process_result_set(result)
    # get column names
    meta = result.getMetaData
    cols = []
    i = 0
    meta.getColumnCount.times{cols << [output_identifier(meta.getColumnLabel(i+=1)), i]}
    @columns = cols.map{|c| c.at(0)}
    row = {}
    blk = if @convert_types
      lambda{|n, i| 
        table = meta.getTableName i
        unless table.nil? || table.to_s.empty? || table.to_s == "sqlite_master"
          col = n
          # puts DB.schema(table).inspect
          column_info = DB.schema(table).select {|c| c[0] == col}.first[1]
          column_type = column_info[:type]
        end
      
        if defined?(column_type) && column_type == :datetime
          obj = result.getObject(i)
          row[n] = obj.nil? ? nil : Time.parse(obj)
        else
          row[n] = convert_type(result.getObject(i))
        end
      }
    else
      lambda{|n, i| row[n] = result.getObject(i)}
    end
    # get rows
    while result.next
      row = {}
      cols.each(&blk)
      yield row
    end
  end
end

DB.extend(SequelDateTimePatch) if defined?(Java)
Sequel::JDBC::SQLite::Dataset.send(:include, SequelDatasetPatch) if defined?(Java)

module SequelModifications
  # module ClassMethods
  #   attr_accessor :immediate_children
  #   
  #   # alias sequel's inherited method, then call it from new inherited method
  #   #alias_method :inherited_original, :inherited
  #   # When a child class is created, add it to the immediate children array
  #   def inherited(subclass)
  #     #inherited_original(subclass)
  #     super
  #     @immediate_children ||= Array.new
  #     @immediate_children << subclass
  #   end
  #   # Return an array of classes containing all children of this class
  #   def child_classes
  #     all_children = Array.new
  #     if @immediate_children
  #       all_children += @immediate_children
  #       @immediate_children.each do |child_class|
  #         all_children += child_class.child_classes
  #       end
  #       all_children
  #     else
  #       Array.new
  #     end
  #   end
  #   def abstract!
  #     @is_abstract = true
  #   end
  #   def abstract?
  #     @is_abstract == true
  #   end
  #   def association_class!
  #     @association_class = true
  #   end
  #   def association_class?
  #     @association_class == true
  #   end
  #   
  #   # Return all instances of this type, including child types
  #   def all &block
  #     all_obj = super
  #     child_classes.each do |klass|
  #       # take the union of both arrays, no duplicates
  #       all_obj |= klass.all &block
  #     end
  #     all_obj
  #   end
  # end
  
  module InstanceMethods
    if defined?(Java)
      def after_initialize
        super
        fix_java_datetimes
      end # End after_initialize hook
      
      def refresh
        return_val = super
        fix_java_datetimes
        return_val
      end
      
      private
      def fix_java_datetimes
        db_schema.each do |column, info_hash|
          next unless info_hash[:type] == :datetime
          val = self[column]
          if val && ([String, Java::java.lang.String].any?{|k| val.kind_of?(k)})
            format = Java::java.text.SimpleDateFormat.new("yyyy-MM-dd HH:mm:ss.SSSSSS")
            new_value = format.parse(val)
            self[column] = new_value if new_value
          end
        end
      end
    end # End if defined?(Java)
  end # End InstanceMethods module
end

class Time
  # Extend the comparision operator so we can compare Java Date objects to
  # Ruby Time objects
  alias_method :orig_compare, :<=> unless public_method_defined?(:orig_compare)
  def <=> other
    defined?(Java) && other.kind_of?(java.util.Date) && \
      other = Time.at(other.time/1000)
    orig_compare other
  end
end

class Sequel::Model
  plugin SequelModifications
end
module UseCaseMetaModelV4
  # ============= Begin class Unit_Of_Work =============
  class Unit_Of_Work < Sequel::Model
    set_dataset :unit_of_works
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    one_to_many :Step, :polymorphic => true

    
  end
  # ============= End class Unit_Of_Work =============
  # ============= Begin class Delta =============
  class Delta < Sequel::Model
    set_dataset :deltas
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    many_to_one :Predicate, :one_to_one => true, :other_prefix => :predicate_guard
    many_to_one :Extension_Scenario
    many_to_one :Step, :polymorphic => true, :other_prefix => :step_resuming_step
    many_to_one :Step, :polymorphic => true, :other_prefix => :step_starting_step
    many_to_many :Step, :through => :Delta_Step_Replacements, :polymorphic => true, :prefix => :delta_replacements, :other_prefix => :step_replacements

    
  end
  # == Begin associated classes for class Delta ==
      
  class Delta_Step_Replacements < Sequel::Model
    set_dataset :delta_step_replacements
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    associates :Delta => :delta_replacement; associates :Step => :step_replacement

    
  end
  

  # == End associated classes for class Delta ==
  # ============= End class Delta =============
  # ============= Begin class User =============
  class User < Sequel::Model
    set_dataset :users
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    one_to_many :Sign_Off
    one_to_many :UseCase, :prefix => :user_champion, :other_prefix => :usecase_champion

    
  end
  # ============= End class User =============
  # ============= Begin class UC_Component =============
  class UC_Component < Sequel::Model
    set_dataset :uc_components
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    # Disabling abstract since tests need to implement constrained (fixing tests is too time consuming)
    # abstract!
    many_to_one :RichText, :as => :UC_Component, :one_to_one => true, :other_prefix => :description
    one_to_many :Assumption, :as => :UC_Component
    one_to_many :Issue, :as => :UC_Component
    one_to_many :NonFunctionalRequirement, :as => :UC_Component
    one_to_many :Business_Rule, :as => :UC_Component
    many_to_many :Reference, :through => :Reference_UC_Component_Association, :as => :UC_Component
    many_to_many :Category, :through => :Category_UC_Component_Association, :as => :UC_Component
    many_to_many :Actor, :through => :Stakeholder, :as => :UC_Component, :polymorphic => true, :prefix => :uc_component_stakeholder, :other_prefix => :actor_stakeholder

    
  end
  # == Begin associated classes for class UC_Component ==
      
  class Reference_UC_Component_Association < Sequel::Model
    set_dataset :reference_uc_component_associations
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    associates :Reference; associates :UC_Component

    
  end
  

      
  class Category_UC_Component_Association < Sequel::Model
    set_dataset :category_uc_component_associations
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    associates :Category; associates :UC_Component

    
  end
  

      
  class Stakeholder < Sequel::Model
    set_dataset :stakeholders
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    association_class!
    associates :Actor => :Stakeholder; associates :UC_Component => :Stakeholder
    many_to_one :RichText, :one_to_one => true, :other_prefix => :interest

    
  end
  

  # == End associated classes for class UC_Component ==
  # ============= End class UC_Component =============
  # ============= Begin class Named =============
  class Named < UC_Component
    set_dataset :nameds
    abstract!

    
  end
  # ============= End class Named =============
  # ============= Begin class Assumption =============
  class Assumption < UC_Component
    set_dataset :assumptions
    many_to_one :UC_Component, :polymorphic => true

    
  end
  # ============= End class Assumption =============
  # ============= Begin class Codeable =============
  class Codeable < Named
    set_dataset :codeables
    many_to_one :Code, :as => :Codeable, :one_to_one => true

    
  end
  # ============= End class Codeable =============
  # ============= Begin class Constrained =============
  class Constrained < Named
    set_dataset :constraineds
    # Disabling abstract since tests need to implement constrained (fixing tests is too time consuming)
    # abstract!
    many_to_one :Algorithm, :as => :Constrained, :one_to_one => true
    one_to_many :Param, :as => :Constrained
    many_to_many :Constraint, :through => :Constrained_Constraint_Constraints, :as => :Constrained, :polymorphic => true, :prefix => :constrained_constraints, :other_prefix => :constraint_constraints
    many_to_many :Actor, :through => :Goal, :as => :Constrained, :polymorphic => true, :prefix => :constrained_goal, :other_prefix => :actor_goal

    
  end
  # == Begin associated classes for class Constrained ==
      
  class Constrained_Constraint_Constraints < Sequel::Model
    set_dataset :constrained_constraint_constraints
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    associates :Constrained => :constraints; associates :Constraint => :constraints

    
  end
  

      
  class Goal < Sequel::Model
    set_dataset :goals
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    association_class!
    associates :Actor => :actor_goal; associates :Constrained => :constrained_goal
    many_to_one :RichText, :one_to_one => true, :other_prefix => :description

    
  end
  

  # == End associated classes for class Constrained ==
  # ============= End class Constrained =============
  # ============= Begin class Constraint =============
  class Constraint < Codeable
    set_dataset :constraints
    abstract!
    many_to_many :Constrained, :through => :Constrained_Constraint_Constraints, :as => :Constraint, :polymorphic => true, :prefix => :constraint_constraints, :other_prefix => :constrained_constraints

    
  end
  # ============= End class Constraint =============
  # ============= Begin class Scenario =============
  class Scenario < Constrained
    set_dataset :scenarios
    many_to_one :UseCase, :as => :Scenario
    one_to_many :Step, :as => :Scenario, :polymorphic => true

    
  end
  # ============= End class Scenario =============
  # ============= Begin class RichText =============
  class RichText < Sequel::Model
    set_dataset :rich_texts
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker

    
  end
  # ============= End class RichText =============
  # ============= Begin class Clause_Holder =============
  class Clause_Holder < Constraint
    set_dataset :clause_holders
    abstract!
    one_to_many :Clause, :as => :Clause_Holder

    
  end
  # ============= End class Clause_Holder =============
  # ============= Begin class Code =============
  class Code < Sequel::Model
    set_dataset :codes
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    many_to_one :RichText, :one_to_one => true, :other_prefix => :code
    one_to_many :Codeable, :polymorphic => true, :one_to_one => true, :other_prefix => :codeable

    
  end
  # ============= End class Code =============
  # ============= Begin class Actor =============
  class Actor < Named
    set_dataset :actors
    many_to_one :Actor, :as => :Actor, :polymorphic => true, :other_prefix => :actor_super
    one_to_many :Actor, :as => :Actor, :polymorphic => true, :prefix => :actor_super, :other_prefix => :actor_sub
    one_to_many :Step, :as => :Actor, :polymorphic => true, :prefix => :actor_actor, :other_prefix => :step_actor
    one_to_many :Regulation, :as => :Actor, :prefix => :actor_agency, :other_prefix => :regulation_agency
    one_to_many :Contract, :as => :Actor, :prefix => :actor_to, :other_prefix => :contract_to
    one_to_many :Contract, :as => :Actor, :prefix => :actor_from, :other_prefix => :contract_from
    many_to_many :Constrained, :through => :Goal, :as => :Actor, :polymorphic => true, :prefix => :actor_goal, :other_prefix => :constrained_goal
    many_to_many :UC_Component, :through => :Stakeholder, :as => :Actor, :polymorphic => true, :prefix => :actor_stakeholder, :other_prefix => :uc_component_stakeholder

    
  end
  # ============= End class Actor =============
  # ============= Begin class Issue =============
  class Issue < UC_Component
    set_dataset :issues
    many_to_one :RichText, :one_to_one => true, :other_prefix => :workarounds
    many_to_one :UC_Component, :polymorphic => true

    
  end
  # ============= End class Issue =============
  # ============= Begin class Contract =============
  class Contract < Clause_Holder
    set_dataset :contracts
    many_to_one :Actor, :polymorphic => true, :other_prefix => :actor_to
    many_to_one :Actor, :polymorphic => true, :other_prefix => :actor_from

    
  end
  # ============= End class Contract =============
  # ============= Begin class Invocation =============
  class Invocation < Sequel::Model
    set_dataset :invocations
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    many_to_one :Param_Use, :one_to_one => true
    one_to_many :Step, :polymorphic => true, :one_to_one => true, :other_prefix => :step
    many_to_one :UseCase, :other_prefix => :usecase_provider

    
  end
  # ============= End class Invocation =============
  # ============= Begin class Param =============
  class Param < Sequel::Model
    set_dataset :params
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    many_to_one :Query, :one_to_one => true, :other_prefix => :query_type
    many_to_one :Constrained, :polymorphic => true
    many_to_many :Query, :through => :Param_Use, :prefix => :param_param_use, :other_prefix => :query_param_use

    
  end
  # == Begin associated classes for class Param ==
      
  class Param_Use < Sequel::Model
    set_dataset :param_uses
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    association_class!
    associates :Param; associates :Query
    one_to_many :Invocation, :one_to_one => true, :other_prefix => :invocation

    
  end
  

  # == End associated classes for class Param ==
  # ============= End class Param =============
  # ============= Begin class Business_Rule =============
  class Business_Rule < Codeable
    set_dataset :business_rules
    many_to_one :UC_Component, :polymorphic => true

    
  end
  # ============= End class Business_Rule =============
  # ============= Begin class Extension_Scenario =============
  class Extension_Scenario < Scenario
    set_dataset :extension_scenarios
    one_to_many :Delta

    
  end
  # ============= End class Extension_Scenario =============
  # ============= Begin class Clause =============
  class Clause < Codeable
    set_dataset :clauses
    many_to_one :Clause_Holder, :polymorphic => true

    
  end
  # ============= End class Clause =============
  # ============= Begin class Ontology =============
  class Ontology < Sequel::Model
    set_dataset :ontologies
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    one_to_many :Domain
    one_to_many :Definition, :prefix => :ontology_glossary, :other_prefix => :definition_glossary

    
  end
  # ============= End class Ontology =============
  # ============= Begin class Category =============
  class Category < Sequel::Model
    set_dataset :categories
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    many_to_one :RichText, :one_to_one => true, :other_prefix => :description
    many_to_one :Category, :other_prefix => :category_parent
    one_to_many :Category, :prefix => :category_parent, :other_prefix => :category_subcat
    many_to_many :UC_Component, :through => :Category_UC_Component_Association, :polymorphic => true

    
  end
  # ============= End class Category =============
  # ============= Begin class Definition =============
  class Definition < Sequel::Model
    set_dataset :definitions
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    many_to_one :RichText, :one_to_one => true, :other_prefix => :definition
    many_to_one :Ontology, :other_prefix => :ontology_glossary

    
  end
  # ============= End class Definition =============
  # ============= Begin class Step =============
  class Step < Constrained
    set_dataset :steps
    many_to_one :Invocation, :as => :Step, :one_to_one => true
    many_to_one :Unit_Of_Work, :as => :Step
    many_to_one :Scenario, :as => :Step, :polymorphic => true
    many_to_one :Actor, :as => :Step, :polymorphic => true, :other_prefix => :actor_actor
    many_to_one :Steps, :as => :Step, :other_prefix => :steps_substeps
    many_to_one :Step, :as => :Step, :polymorphic => true, :other_prefix => :step_variants
    one_to_many :Query, :as => :Step, :prefix => :step_effects, :other_prefix => :query_effects
    one_to_many :Delta, :as => :Step, :prefix => :step_resuming_step, :other_prefix => :delta_resuming_step
    one_to_many :Delta, :as => :Step, :prefix => :step_starting_step, :other_prefix => :delta_starting_step
    one_to_many :Step, :as => :Step, :polymorphic => true, :prefix => :step_variants, :other_prefix => :step_variants
    many_to_many :Delta, :through => :Delta_Step_Replacements, :as => :Step, :prefix => :step_replacements, :other_prefix => :delta_replacements

    
  end
  # ============= End class Step =============
  # ============= Begin class Predicate =============
  class Predicate < Constraint
    set_dataset :predicates
    one_to_many :Delta, :one_to_one => true, :prefix => :predicate_guard, :other_prefix => :delta_guard

    
  end
  # ============= End class Predicate =============
  # ============= Begin class Steps =============
  class Steps < Step
    set_dataset :steps_table
    one_to_many :Step, :polymorphic => true, :prefix => :steps_substeps, :other_prefix => :step_substeps

    
  end
  # ============= End class Steps =============
  # ============= Begin class NonFunctionalRequirement =============
  class NonFunctionalRequirement < UC_Component
    set_dataset :non_functional_requirements
    many_to_one :UC_Component, :polymorphic => true

    
  end
  # ============= End class NonFunctionalRequirement =============
  # ============= Begin class Domain =============
  class Domain < Constrained
    set_dataset :domains
    many_to_one :Ontology
    many_to_many :System, :through => :Domain_System_Association

    
  end
  # == Begin associated classes for class Domain ==
      
  class Domain_System_Association < Sequel::Model
    set_dataset :domain_system_associations
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    associates :Domain; associates :System

    
  end
  

  # == End associated classes for class Domain ==
  # ============= End class Domain =============
  # ============= Begin class Reference =============
  class Reference < UC_Component
    set_dataset :references
    many_to_many :UC_Component, :through => :Reference_UC_Component_Association, :polymorphic => true

    
  end
  # ============= End class Reference =============
  # ============= Begin class Query =============
  class Query < Codeable
    set_dataset :queries
    one_to_many :Param, :one_to_one => true, :prefix => :query_type, :other_prefix => :param_type
    many_to_one :Step, :polymorphic => true, :other_prefix => :step_effects
    many_to_many :Param, :through => :Param_Use, :prefix => :query_param_use, :other_prefix => :param_param_use

    
  end
  # ============= End class Query =============
  # ============= Begin class System =============
  class System < Actor
    set_dataset :systems
    one_to_many :UseCase
    many_to_many :Domain, :through => :Domain_System_Association

    
  end
  # ============= End class System =============
  # ============= Begin class Algorithm =============
  class Algorithm < Codeable
    set_dataset :algorithms
    one_to_many :Constrained, :polymorphic => true, :one_to_one => true, :other_prefix => :constrained

    
  end
  # ============= End class Algorithm =============
  # ============= Begin class Regulation =============
  class Regulation < Clause_Holder
    set_dataset :regulations
    many_to_one :Actor, :polymorphic => true, :other_prefix => :actor_agency

    
  end
  # ============= End class Regulation =============
  # ============= Begin class UseCase =============
  class UseCase < Constrained
    set_dataset :use_cases
    many_to_one :RichText, :one_to_one => true, :other_prefix => :narrative
    many_to_one :System
    many_to_one :UseCase, :other_prefix => :usecase_base
    many_to_one :User, :other_prefix => :user_champion
    many_to_one :UseCase, :other_prefix => :usecase_general
    one_to_many :Invocation, :prefix => :usecase_provider, :other_prefix => :invocation_provider
    one_to_many :Scenario, :polymorphic => true
    one_to_many :UseCase, :prefix => :usecase_base, :other_prefix => :usecase_extensions
    one_to_many :Sign_Off
    one_to_many :UseCase, :prefix => :usecase_general, :other_prefix => :usecase_specializations

    
  end
  # ============= End class UseCase =============
  # ============= Begin class Sign_Off =============
  class Sign_Off < Sequel::Model
    set_dataset :sign_offs
    plugin :boolean_readers
    plugin :specific_associations
    plugin :change_tracker
    many_to_one :User
    many_to_one :UseCase

    
  end
  # ============= End class Sign_Off =============
end