require File.dirname(__FILE__) + '/spec_helper.rb'

describe ChangeTracker, "enumerations work" do
  before(:each) do
    ChangeTracker.cancel
    clear_db
  end
  after(:each) do
    setup_session
  end
  
  it "allow the initialization of an attribute that is typed as an enumeration during new object creation" do
    ChangeTracker.start
    bear = Woods::Bear.create(:name => 'Tina', :fur_color => 'brown')
    ChangeTracker.commit.should be true
    expect(bear.name).to eq 'Tina'
    expect(bear.fur_color.value).to eq 'brown'
  end
  
  # NOTE This test fails when you try to create a new bear with two enumeration attributes AND another attribute.  It is fine if the only this you initialize at creation are the two enumeration attributes.
  it "allow the initialization of multiple attributes that are typed as enumerations during new object creation" do
    ChangeTracker.start
    bear = Woods::Bear.create(:fur_color => 'brown', :gender => 'male')
    ChangeTracker.commit.should be true
    expect(bear.fur_color.value).to eq 'brown'
    expect(bear.gender.value).to eq 'male'
    
    # failing part here!
    ChangeTracker.start
    bear = Woods::Bear.create(:name => 'Tina', :fur_color => 'brown', :gender => 'female')
    ChangeTracker.commit.should be true
    expect(bear.name).to eq 'Tina'
    expect(bear.fur_color.value).to eq 'brown'
    expect(bear.gender.value).to eq 'female'
  end
end
