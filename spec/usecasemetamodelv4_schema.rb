module UseCaseMetaModelV4
  # ============= Begin class Unit_Of_Work =============
  DB.create_table :unit_of_works do
    String :name
    primary_key :id
  end
  # ============= End class Unit_Of_Work =============
  # ============= Begin class Delta =============
  DB.create_table :deltas do
    foreign_key :extension_scenario_id, :extension_scenarios, :type => Integer
    foreign_key :predicate_guard_id, :predicates, :type => Integer
    Integer :step_resuming_step_id
    String :step_resuming_step_class
    Integer :step_starting_step_id
    String :step_starting_step_class
    primary_key :id
  end
  # == Begin associated classes for class Delta ==
      
  DB.create_table :delta_step_replacements do
    Integer :delta_replacements_id
    Integer :step_replacements_id
    String :step_replacements_class
    primary_key :id
    index [:delta_replacements_id, :step_replacements_id, :step_replacements_class]
  end
  

  # == End associated classes for class Delta ==
  # ============= End class Delta =============
  # ============= Begin class User =============
  DB.create_table :users do
    TrueClass :active
    primary_key :id
  end
  # ============= End class User =============
  # ============= Begin class UC_Component =============
  DB.create_table :uc_components do
    foreign_key :description_id, :rich_texts, :type => Integer
    primary_key :id
  end
  # == Begin associated classes for class UC_Component ==
      
  DB.create_table :reference_uc_component_associations do
    Integer :reference_id
    Integer :uc_component_id
    String :uc_component_class
    primary_key :id
    index [:reference_id, :uc_component_id, :uc_component_class]
  end
  

      
  DB.create_table :category_uc_component_associations do
    Integer :category_id
    Integer :uc_component_id
    String :uc_component_class
    primary_key :id
    index [:category_id, :uc_component_id, :uc_component_class]
  end
  

      
  DB.create_table :stakeholders do
    Integer :actor_stakeholder_id
    String :actor_stakeholder_class
    Integer :uc_component_stakeholder_id
    String :uc_component_stakeholder_class
    foreign_key :interest_id, :rich_texts, :type => Integer
    primary_key :id
    index [:actor_stakeholder_id, :actor_stakeholder_class, :uc_component_stakeholder_id, :uc_component_stakeholder_class]
  end
  

  # == End associated classes for class UC_Component ==
  # ============= End class UC_Component =============
  # ============= Begin class Named =============
  DB.create_table :nameds do
    String :name
    foreign_key :description_id, :rich_texts, :type => Integer
    primary_key :id
  end
  # ============= End class Named =============
  # ============= Begin class Assumption =============
  DB.create_table :assumptions do
    foreign_key :description_id, :rich_texts, :type => Integer
    Integer :uc_component_id
    String :uc_component_class
    primary_key :id
  end
  # ============= End class Assumption =============
  # ============= Begin class Codeable =============
  DB.create_table :codeables do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :code_id, :codes, :type => Integer
    primary_key :id
  end
  # ============= End class Codeable =============
  # ============= Begin class Constrained =============
  DB.create_table :constraineds do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :algorithm_id, :algorithms, :type => Integer
    primary_key :id
  end
  # == Begin associated classes for class Constrained ==
      
  DB.create_table :constrained_constraint_constraints do
    Integer :constrained_constraints_id
    String :constrained_constraints_class
    Integer :constraint_constraints_id
    String :constraint_constraints_class
    primary_key :id
    index [:constrained_constraints_id, :constrained_constraints_class, :constraint_constraints_id, :constraint_constraints_class]
  end
  

      
  DB.create_table :goals do
    Integer :actor_goal_id
    String :actor_goal_class
    Integer :constrained_goal_id
    String :constrained_goal_class
    foreign_key :description_id, :rich_texts, :type => Integer
    primary_key :id
    index [:actor_goal_id, :actor_goal_class, :constrained_goal_id, :constrained_goal_class]
  end
  

  # == End associated classes for class Constrained ==
  # ============= End class Constrained =============
  # ============= Begin class Constraint =============
  DB.create_table :constraints do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :code_id, :codes, :type => Integer
    primary_key :id
  end
  # ============= End class Constraint =============
  # ============= Begin class Scenario =============
  DB.create_table :scenarios do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :usecase_id, :use_cases, :type => Integer
    foreign_key :algorithm_id, :algorithms, :type => Integer
    primary_key :id
  end
  # ============= End class Scenario =============
  # ============= Begin class RichText =============
  DB.create_table :rich_texts do
    String :content
    String :markup_language
    primary_key :id
  end
  # ============= End class RichText =============
  # ============= Begin class Clause_Holder =============
  DB.create_table :clause_holders do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :code_id, :codes, :type => Integer
    primary_key :id
  end
  # ============= End class Clause_Holder =============
  # ============= Begin class Code =============
  DB.create_table :codes do
    foreign_key :code_id, :rich_texts, :type => Integer
    String :language
    primary_key :id
  end
  # ============= End class Code =============
  # ============= Begin class Actor =============
  DB.create_table :actors do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    Integer :actor_super_id
    String :actor_super_class
    primary_key :id
  end
  # ============= End class Actor =============
  # ============= Begin class Issue =============
  DB.create_table :issues do
    String :category
    Float :severity
    String :status
    foreign_key :workarounds_id, :rich_texts, :type => Integer
    foreign_key :description_id, :rich_texts, :type => Integer
    Integer :uc_component_id
    String :uc_component_class
    primary_key :id
  end
  # ============= End class Issue =============
  # ============= Begin class Contract =============
  DB.create_table :contracts do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    Integer :actor_to_id
    String :actor_to_class
    Integer :actor_from_id
    String :actor_from_class
    foreign_key :code_id, :codes, :type => Integer
    primary_key :id
  end
  # ============= End class Contract =============
  # ============= Begin class Invocation =============
  DB.create_table :invocations do
    String :cat
    foreign_key :usecase_provider_id, :use_cases, :type => Integer
    foreign_key :param_use_id, :param_uses, :type => Integer
    primary_key :id
  end
  # ============= End class Invocation =============
  # ============= Begin class Param =============
  DB.create_table :params do
    String :name
    Integer :constrained_id
    String :constrained_class
    foreign_key :query_type_id, :queries, :type => Integer
    primary_key :id
  end
  # == Begin associated classes for class Param ==
      
  DB.create_table :param_uses do
    Integer :param_param_use_id
    Integer :query_param_use_id
    primary_key :id
    index [:param_param_use_id, :query_param_use_id]
  end
  

  # == End associated classes for class Param ==
  # ============= End class Param =============
  # ============= Begin class Business_Rule =============
  DB.create_table :business_rules do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    Integer :uc_component_id
    String :uc_component_class
    foreign_key :code_id, :codes, :type => Integer
    primary_key :id
  end
  # ============= End class Business_Rule =============
  # ============= Begin class Extension_Scenario =============
  DB.create_table :extension_scenarios do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :usecase_id, :use_cases, :type => Integer
    foreign_key :algorithm_id, :algorithms, :type => Integer
    primary_key :id
  end
  # ============= End class Extension_Scenario =============
  # ============= Begin class Clause =============
  DB.create_table :clauses do
    String :type
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    Integer :clause_holder_id
    String :clause_holder_class
    foreign_key :code_id, :codes, :type => Integer
    primary_key :id
  end
  # ============= End class Clause =============
  # ============= Begin class Ontology =============
  DB.create_table :ontologies do
    String :uml_model_url
    primary_key :id
  end
  # ============= End class Ontology =============
  # ============= Begin class Category =============
  DB.create_table :categories do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :category_parent_id, :categories, :type => Integer
    primary_key :id
  end
  # ============= End class Category =============
  # ============= Begin class Definition =============
  DB.create_table :definitions do
    foreign_key :definition_id, :rich_texts, :type => Integer
    String :term
    foreign_key :ontology_glossary_id, :ontologies, :type => Integer
    primary_key :id
  end
  # ============= End class Definition =============
  # ============= Begin class Step =============
  DB.create_table :steps do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :unit_of_work_id, :unit_of_works, :type => Integer
    Integer :scenario_id
    String :scenario_class
    Integer :actor_actor_id
    String :actor_actor_class
    foreign_key :steps_substeps_id, :steps_table, :type => Integer
    foreign_key :invocation_id, :invocations, :type => Integer
    Integer :step_variants_id
    String :step_variants_class
    foreign_key :algorithm_id, :algorithms, :type => Integer
    primary_key :id
  end
  # ============= End class Step =============
  # ============= Begin class Predicate =============
  DB.create_table :predicates do
    String :semantics
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :code_id, :codes, :type => Integer
    primary_key :id
  end
  # ============= End class Predicate =============
  # ============= Begin class Steps =============
  DB.create_table :steps_table do
    String :semantics
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :unit_of_work_id, :unit_of_works, :type => Integer
    Integer :scenario_id
    String :scenario_class
    Integer :actor_actor_id
    String :actor_actor_class
    foreign_key :steps_substeps_id, :steps_table, :type => Integer
    foreign_key :invocation_id, :invocations, :type => Integer
    Integer :step_variants_id
    String :step_variants_class
    foreign_key :algorithm_id, :algorithms, :type => Integer
    primary_key :id
  end
  # ============= End class Steps =============
  # ============= Begin class NonFunctionalRequirement =============
  DB.create_table :non_functional_requirements do
    String :type
    foreign_key :description_id, :rich_texts, :type => Integer
    Integer :uc_component_id
    String :uc_component_class
    primary_key :id
  end
  # ============= End class NonFunctionalRequirement =============
  # ============= Begin class Domain =============
  DB.create_table :domains do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :ontology_id, :ontologies, :type => Integer
    foreign_key :algorithm_id, :algorithms, :type => Integer
    primary_key :id
  end
  # == Begin associated classes for class Domain ==
      
  DB.create_table :domain_system_associations do
    Integer :domain_id
    Integer :system_id
    primary_key :id
    index [:domain_id, :system_id]
  end
  

  # == End associated classes for class Domain ==
  # ============= End class Domain =============
  # ============= Begin class Reference =============
  DB.create_table :references do
    String :semantics
    String :uri
    foreign_key :description_id, :rich_texts, :type => Integer
    primary_key :id
  end
  # ============= End class Reference =============
  # ============= Begin class Query =============
  DB.create_table :queries do
    String :data_name
    String :query_semantics
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    Integer :step_effects_id
    String :step_effects_class
    foreign_key :code_id, :codes, :type => Integer
    primary_key :id
  end
  # ============= End class Query =============
  # ============= Begin class System =============
  DB.create_table :systems do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    Integer :actor_super_id
    String :actor_super_class
    primary_key :id
  end
  # ============= End class System =============
  # ============= Begin class Algorithm =============
  DB.create_table :algorithms do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :code_id, :codes, :type => Integer
    primary_key :id
  end
  # ============= End class Algorithm =============
  # ============= Begin class Regulation =============
  DB.create_table :regulations do
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    Integer :actor_agency_id
    String :actor_agency_class
    foreign_key :code_id, :codes, :type => Integer
    primary_key :id
  end
  # ============= End class Regulation =============
  # ============= Begin class UseCase =============
  DB.create_table :use_cases do
    Integer :executions_per_year
    String :goal_level
    Float :implementation_priority
    foreign_key :narrative_id, :rich_texts, :type => Integer
    String :reason
    foreign_key :description_id, :rich_texts, :type => Integer
    String :name
    foreign_key :system_id, :systems, :type => Integer
    foreign_key :usecase_base_id, :use_cases, :type => Integer
    foreign_key :user_champion_id, :users, :type => Integer
    foreign_key :usecase_general_id, :use_cases, :type => Integer
    foreign_key :algorithm_id, :algorithms, :type => Integer
    primary_key :id
  end
  # ============= End class UseCase =============
  # ============= Begin class Sign_Off =============
  DB.create_table :sign_offs do
    Time :_when
    foreign_key :user_id, :users, :type => Integer
    foreign_key :usecase_id, :use_cases, :type => Integer
    primary_key :id
  end
  # ============= End class Sign_Off =============
end