require_relative 'sequel_change_tracker/sql/schemas'
require_relative 'sequel_change_tracker/sql/migration'
require_relative 'sequel_change_tracker/model/init'
require_relative 'sequel_change_tracker/controller/cache'
require_relative 'sequel_change_tracker/controller/session'
require_relative 'sequel_change_tracker/controller/controller'
require_relative 'sequel_change_tracker/sequel_object'
require_relative 'sequel_change_tracker/meta_info'

# Require after_initialize plugin
Sequel::Model.plugin :after_initialize