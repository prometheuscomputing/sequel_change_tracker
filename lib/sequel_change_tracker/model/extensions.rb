require 'set'

# Extend array to add move functionality
# Credit: John Eberly
# http://www.elctech.com/snippets/moving-items-within-a-ruby-array
class Array
  def move(from, to)
    insert(to, delete_at(from))
  end

  def move_by_name(name, to)
    return self if index(name).nil? # return original array if name not found
    move(index(name), to)
  end
end

class String
  def to_class
    self.split(/\.|::/).inject(Object) {|scope, const_name| scope.const_get(const_name)}
  end
end

class Float
  def round_to_digits(num)
    (self * 10**num).round.to_f / 10**num
  end
  def truncate_to_digits(num)
    (self * 10**num).truncate.to_f / 10**num
  end
        
end