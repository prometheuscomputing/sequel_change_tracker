module ChangeTracker
  # default ChangeTracker error from which others inherit
  class Error < StandardError
  end
  
  # Raised if a command is issued to the ChangeTracker before the session hash is set
  class NoSessionHashError < Error
  end
  
  # Error due to the change tracker not having been started
  class NotStartedError < Error
  end
  
  # Error due to the basis changing
  class BasisChangedError < Error
  end
  
  # Error due to a failure to apply changes
  class ApplyError < Error
  end
  
  # Raised when a change is requested which is invalid
  class InvalidOperation < Error
  end
  
  # Raised when unknown semantics are requested for a change type
  class InvalidSemanticsError < Error
  end
end
    