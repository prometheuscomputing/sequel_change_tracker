module ChangeTracker
  def self.define_models
    models = %w(extensions change deleted_item history identity problem unit_of_work errors sequel_model)
    models.each do |model|
      require File.join(File.dirname(__FILE__), model)
    end
  end
end