module ChangeTracker
  ChangeLifecycleSemantics = [
    :Create,
    :Destroy]
  
  ChangeAssociationSemantics = [
    :Add,
    :Remove]
  
  # ------------------------------------------------------------------------------
  # Change - Base class for all types of changes to an object
  # ------------------------------------------------------------------------------
  class Change < Sequel::Model
    set_dataset :ct_changes
    plugin :single_table_inheritance, :change_type
    attr_accessor :problems
    attr_accessor :targets
    
    def targets
      @targets ||= []
    end
    def target
      self.targets.first
    end
    def target=(t)
      self.targets = [t]
    end

    def after_initialize
      super
      @problems = new? ? [] : Problem.filter(:change_id => pk).all
    end
    
    def unit_of_work
      UnitOfWork[self.unit_of_work_id]
    end
    
    # Do the change to the change's target
    def do
      begin
        num_changes = do_to(target)
        self.unit_of_work_id = target.ct_unit_of_work.id
        num_changes
      rescue Sequel::Error => e
        puts "Encountered Sequel::Error while attempting to apply Change: #{self.inspect}"
        puts "Association Operation was: #{self.association_operation.inspect}" if self.is_a?(ChangeAssociation)
        puts "On Identity: #{Identity[self.identity_id].inspect}" if self.identity_id
        raise e
      end
    end
    
    # These types of changes will be persisted to the DB
    def do_to(target); raise "Failed to override method do_to."; end
    # Undo changes are not persisted. They only return the object before the change occurred
    def undo_to(target); raise "Failed to override method undo_to."; end
    
    def save_problems
      @problems.each do |problem|
        problem.change_id = self.pk
        problem.save
      end
    end
  
    def save
      super
      save_problems
    end
  end

  # ------------------------------------------------------------------------------
  # ChangeAttribute - Describes a change made to an attribute of an object
  # ------------------------------------------------------------------------------
  class ChangeAttribute < Change
    # These attributes are added by the schema
    #attr_reader :attribute_name
    #attr_reader :new_value
    #attr_reader :old_value
  
    def getter; "#{self.attribute_name}".to_sym; end
    def setter; "#{self.attribute_name}=".to_sym; end
    
    def self.build(target, column, old_value, new_value)
      # creating ChangeAttribute event -- Name: #{column} Values: #{old_value.inspect} => #{new_value.inspect}
      change = ChangeAttribute.new
      change.init(target, column, old_value, new_value)
      change
    end
    
    def init(target, column, old_value, new_value)
      # Set columns
      self.attribute_name = column.to_s
      # TODO: write support for storing column type and encoding (via YAML?)
      self.old_value = parse_input_value(old_value)
      self.new_value = parse_input_value(new_value)
      self.identity_id = target.ct_identity.id
      self.target = target
      self
    end
    
    def old_value
      parse_output_value(self[:old_value])
    end
    
    def new_value
      parse_output_value(self[:new_value])
    end
    
    # Apply the change to the domain object
    def do_to(target)
      # Applying ChangeAttribute
      # NOTE: this relies on Sequel's typecasting function to appropriately convert the attribute from
      #       a blob (Sequel::SQL::Blob) to the correct type.
      target[attribute_name.to_sym] = self.new_value
      # about to save target to db
      # TODO: find an inexpensive way to avoid doing this for every ChangeAttribute
      target.save_original
        
      # saving ChangeAttribute
      save unless ChangeTracker.limited_mode
      return 1
    end
  
    # Undo the change done to the domain object
    def undo_to(target)
      target[attribute_name.to_sym] = self.old_value
      return 1
    end
    
    def to_s(options = {})
      str = []
      str << "ChangeAttribute:"
      str << "  attribute_name=>#{attribute_name}"
      str << "  old_value=>#{old_value.inspect}"
      str << "  new_value=>#{new_value.inspect}"
      str.join#("\n")
    end
    
    private
    # Ensure that values used to build this ChangeAttribute are UTF-8 if strings, converted to UTF-8 strings if not Strings, or left alone if nil
    def parse_input_value(value)
      case value
      when NilClass
        nil
      else
        value.to_s.dup.force_encoding(Encoding::UTF_8)        
      end
    end
    
    # Ensure that when values are retrieved from this
    def parse_output_value(value)
      case value
      when NilClass
        nil
      when String
        value.dup.force_encoding(Encoding::UTF_8)
      end
    end
  end

  # ------------------------------------------------------------------------------
  # ChangeLifecycle - Describes a change made to the lifecycle of an object
  # ------------------------------------------------------------------------------
  class ChangeLifecycle < Change
    
    def self.build(target, semantics)
      change = ChangeLifecycle.new(:semantics => semantics.to_s, :identity_id => target.ct_history.identity.id)
      change.init(target, semantics)
      change
    end
    
    def init(target, semantics)
      self.target = target
      self.validate
      self
    end
  
    def validate
      unless ChangeLifecycleSemantics.include?(self.semantics.to_sym)
        raise InvalidSemantics, "Invalid semantics for ChangeLifecycle. Received #{self.semantics.to_s}"
      end
    end
  
    def disable; :disable;  end
    def enable; :enable; end
  
    # Apply the change to the domain object
    def do_to(target)
      # Creating/Destroying target
      case self.semantics.to_sym
      when :Create
        # Applying Create change
        if target.deleted?
          target.send(enable) # needed for un-deleting objects
        else # target is a newly created object
          # save target to db
          target.save_original
        end
      when :Destroy
        # Applying Destroy change
        # disable the DomainObject unless already deleted
        if target.deleted?
          puts "ChangeTracker:: Warning: Attempted to destroy already deleted object #{target.inspect}"
        else
          target.send(disable)
        end
      else
        raise InvalidSemantics, "Invalid semantics for ChangeLifecycle. Received #{self.semantics.to_s}"
      end
      # about to save ChangeLifecycle
      save unless ::ChangeTracker.extra_limited_mode
      return 1
    end
  
    # Undo the change done to the domain object
    def undo_to(target)
      case self.semantics.to_sym
      when :Create
        target.obj_deleted = true
      when :Destroy
        target.obj_deleted = false
      else
        raise InvalidSemantics, "Invalid semantics for ChangeLifecycle. Received #{self.semantics.to_s}"
      end
      return 1
    end
    
    def to_s(options = {})
      str = []
      str << "ChangeLifecycle: type=>#{semantics} class=>#{target.class}(#{target.ct_identity.id})"
      str.join("\n")
    end
  end
  
  # A complex change type. Is persisted, and creates smaller
  # attribute change types that are also persisted.
  class ChangeAssociation < Change
    attr_accessor :association_operation
    # ChangeAssociation persistance attributes
    # attr_accessor :semantics, :getter, :model_identity_id, :second_model_identity_id
    
    def self.build(target, association_operation)
      association_change = ChangeAssociation.new
      # Initialize ChangeAssociation, making ChangeLifecycle changes if necessary
      save_changes = association_change.init(target, association_operation)
      save_changes << association_change
    end
    
    def init(unused_target, association_operation)
      self.association_operation = association_operation
      self.semantics = association_operation.is_a?(SpecificAssociations::PendingAssociationRemoval) ? 'Remove' : 'Add'
      self.model_identity_id = association_operation.model.ct_identity.id
      self.second_model_identity_id = association_operation.second_model.ct_identity.id
      self.getter = association_operation.association[:getter].to_s
      
      save_changes = self.save_models_if_required
      save_changes
    end
    
    # Helper method to retreive the correct association information from the perspective of the passed domain_obj
    # This method is used externally by gui_site for the History feature
    def assoc_info_for(domain_obj)
      role, assoc_info = domain_obj_role_info(domain_obj)
      assoc_info
    end
    
    def domain_obj_role_info(domain_obj)
      case domain_obj.ct_identity.id
      when model_identity_id
        [:model, domain_obj.class.associations[self.getter.to_sym]]
      when second_model_identity_id
        [:second_model, domain_obj.class.associations.values.find{|info| info[:opp_getter] == self.getter.to_sym}]
      when identity_id
        [:join, nil]
      else
        raise "Can't get domain object role and information for #{domain_obj.inspect} for change: #{self.inspect}\n" +
          "Received identity_id: #{domain_obj.ct_identity.id}. Valid values: #{model_identity_id}, #{second_model_identity_id}, #{identity_id}"
      end
    end
    
    # Ensure this is a valid operation to add to current unit of work
    # If adding an association for models without an id, and no creation change pending,
    # automatically add an appropriate creation change.
    def save_models_if_required
      changes = []
      if association_operation.is_a?(SpecificAssociations::PendingAssociationAddition)
        models = [association_operation.second_model]
        models << association_operation.model if association_operation.through_klass
        
        models.each do |m|
          # No action required if model has a primary key
          next if m.pk
          # No pk, must check for Create ChangeLifecycle
          next if m.ct_unit_of_work.created_objects.any?{|co| co.equal?(m)}
          # No primary key and no Create change. Must insert create event.
          changes << ChangeLifecycle.build(m, :Create)
        end
      end
      changes
    end
    
    # Target of a change association is entirely unused. All information about the association is contained
    #   within the association operation
    def do_to(unused_target)
      num_changes_applied = 0
      modified_objs = association_operation.apply
      # The modified object is the join entry if the association is many-to-many
      join = association_operation.join
      if join
        lifecycle_semantics = (self.semantics.to_sym == :Add) ? :Create : :Destroy
        change = ChangeLifecycle.build(join, lifecycle_semantics)
        change.position = self.position
        change.do
        change.save unless ::ChangeTracker.extra_limited_mode
        modified_objs.delete(join)
        num_changes_applied += 1
      end
      
      self.targets = [association_operation.model, association_operation.second_model]
      self.targets << join if join
      
      # Save the modified objects (column modifications to persist association and reordering changes)
      modified_objs.each{|o|
        o.save_original
        # Prevent column changes for association from being parsed as attribute changes by later save operations
        o.reset_changed_columns
      }

      # Persist ChangeAssociation
      self.identity_id = join ? join.ct_identity.id : second_model_identity_id
      # Change position to occur after generated changes
      self.position += num_changes_applied
      self.save unless ChangeTracker.limited_mode
      num_changes_applied
    end
    
    # Undo the change done to the domain object in a non-persistent manner
    def undo_to(target)
      target_role, target_assoc_info = domain_obj_role_info(target)
      # Currently only rewinding association for key-holding class (second_model if not many_to_many)
      return 0 unless target_assoc_info && target_assoc_info[:type] == :many_to_one # target_role == :second_model is implied
      getter = target_assoc_info[:getter]
      nontarget_identity_id = (target_role == :model) ? second_model_identity_id : model_identity_id
      # NOTE: if nontarget is not change tracked, this may fail
      # TODO: add testing for associations between change-tracked and non-change-tracked objects
      nontarget = ChangeTracker::Identity[nontarget_identity_id].object
      id_col = (getter.to_s + '_id').to_sym
      class_col = (getter.to_s + '_class').to_sym
      complex_getter_col = (getter.to_s + '_complex_getter')
      class_col = nil unless target.columns.include?(class_col)
      # AFAIK, this code is untested. I'm not really sure if it's likely to be needed, but I'm adding it anyway
      #   so you don't have to figure out how to write this part from scratch.
      # The complex_getter is only needed when dealing with a proxy association from the owner of a complex attribute
      #   to an association of that complex attribute, and only when the complex attribute owner is not the key-holder. -SD
      complex_getter_col = nil unless target.columns.include?(complex_getter_col) && target_assoc_info[:for_complex_attribute]
      complex_getter = target_assoc_info[:for_complex_attribute]
      args = [target, target_assoc_info, id_col, class_col, nil, nontarget, nil, nil, nil, nil, complex_getter_col, complex_getter]
      case self.semantics.to_sym
      when :Add # Remove the association (ignoring any positional reordering)
        op = SpecificAssociations::PendingAssociationRemoval.new(*args)
      when :Remove # Add the association
        op = SpecificAssociations::PendingAssociationAddition.new(*args)
      end
      op.apply
      return 1
    end
    
    # Whether or not this change is applicable to the association between the given models with the singularized getter
    # It can be assumed that model1 is change tracked, but model2 may or may not be tracked.
    def applies_to?(model1, getter, model2)
      if association_operation.pk_field.to_s == "#{getter}_id" && association_operation.model.ct_identity == model1.ct_identity
        if model2.respond_to?(:ct_identity) # model2 is change tracked
          return association_operation.second_model.ct_identity == model2.ct_identity
        else # Unable to compare ct_identities, so fall back to comparing models. This can be inaccurate.
          return association_operation.second_model == model2
        end
      elsif association_operation.second_pk_field.to_s == "#{getter}_id" && association_operation.second_model.ct_identity == model1.ct_identity
        if model2.respond_to?(:ct_identity) # model2 is change tracked
          return association_operation.model.ct_identity == model2.ct_identity
        else # Unable to compare ct_identities, so fall back to comparing models. This can be inaccurate.
          return association_operation.model == model2
        end
      end
    end
    
    def to_s(options = {:lookup_old_associations => true})
      str = []
      case association_operation
      when SpecificAssociations::PendingAssociationAddition
        operation_type = "Addition"
        is_addition = true
      when SpecificAssociations::PendingAssociationRemoval
        operation_type = "Removal"
        is_addition = false
      else
        raise ArgumentError, "Invalid Pending Association type. Received type #{association_operation.class.to_s}"
      end
      
      through = association_operation.through_klass
      model = association_operation.model
      pk_field = association_operation.pk_field
      type_field = association_operation.type_field
      second_model = association_operation.second_model
      second_pk_field = association_operation.second_pk_field
      second_type_field = association_operation.second_type_field
      
      str << "ChangeAssociation: type=>#{operation_type} class1=>#{model.class}(#{model.ct_identity.id}) class2=>#{second_model.class}(#{second_model.ct_identity.id})"
      str << "    pk_field=>#{pk_field}" + (", type_field=>#{type_field}" if type_field).to_s
      str << "    pk_field=>#{second_pk_field}" + (", type_field=>#{second_type_field}" if second_type_field).to_s
      str.join("\n")
    end
  end
end
