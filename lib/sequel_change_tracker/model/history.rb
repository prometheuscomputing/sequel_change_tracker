module ChangeTracker
  # History provides methods to the user that allow interaction with the DomainObject histories
  # 
  # All timestamps are microsecond precision, so be sure not to lose precision by calling
  # the function Time.at(timestamp.to_i) which doesn't carry uSecs. instead call something
  # like Time.at(timestamp.to_i, timestamp.usec)
  # Also, on some systems (Ubuntu for one) Time.now returns a nanosecond precision timestamp
  # (under ruby 1.9.1). Nanoseconds are not used for these timestamps, and should be removed
  # (e.g. Time.at(timestamp.to_i, timestamp.usec) will not carry nanoseconds)
  class History
    # Link to newest version of the associated DomainObject
    attr_accessor :newest
    #attr_accessor :changes
    
    def self.history_from_identity_id(identity_id)
      History.new(ChangeTracker::Identity[identity_id].object)
    end
  
    def initialize(domain_object)
      @newest = domain_object
    end
    
    # Return an unsorted list of applicable changes
    def unsorted_change_query(use_model = true)
      identity_id = identity.id
      query_base = use_model ? Change : Change.naked
      query_base.filter(Sequel.|({:identity_id => identity_id},{:model_identity_id => identity_id},{:second_model_identity_id => identity_id}))
    end
    
    # Returns a query of changes associated with this history, sorted by uow completion_time and then by position in the uow
    def change_query(use_model = true)
      sorted_query = unsorted_change_query(use_model).join(:ct_units_of_work, :id => :unit_of_work_id).order(:completion_time).order_more(:position)
      # Select only change columns and uow completion time and author
      # Currently not selecting submission_time and basis_readtime from unit of work.
      sorted_query.select(Sequel.expr(:ct_changes).*, :ct_units_of_work__completion_time, :ct_units_of_work__author)
    end
    
    # Return a query of units of work associated with this history, sorted by completion time
    def uow_query(use_model = true)
      query = use_model ? UnitOfWork : UnitOfWork.naked
      query = query.where(:id => unsorted_change_query(false).select(:unit_of_work_id))
      query.order(:completion_time)
    end
    
    # Returns an array of Change objects
    def changes
      change_query.all
    end
    
    # Returns an array of UnitOfWork objects
    def units_of_work
      uow_query.all
    end
  
    # Returns an array of timestamps corresponding to the available versions
    def versions
      uow_query(false).select(:completion_time).all.collect {|uow| uow[:completion_time] }
    end
    
    # A more efficient calculation to get the latest version
    def last_version
      v = uow_query(false).select(:completion_time).last
      v[:completion_time] if v
    end
    
    # Returns the last timestamp version that exists before the given timestamp
    def version_time_before(timestamp)
      versions.reverse.each do |version|
        return version if version < timestamp
      end
      nil
    end
  
    # Returns the version of the DomainObject at the given timestamp
    # Any changes that occurred at the same time as the given timestamp will have been applied
    # If timestamp is nil, then set to oldest available version
    def version_at(timestamp)
      obj = identity.object
      obj.obj_deleted = identity.deleted
      needed_changes = timestamp.nil? ? changes : changes_since(timestamp)
      # Iterate changes, being sure to put them in newest->oldest order first
      needed_changes.reverse.each do |change|
        change.undo_to(obj)
      end
      # TODO: Am i using the version of the returned object?
      # TODO: should I set the identity of the object to the value of obj_deleted ?
      # Return nil if object was deleted (and we didn't ask for the oldest existing object by specifying timestamp = nil)
      obj.obj_deleted == true && !timestamp.nil? ? nil : obj
    end
    
    # TODO: see if this method works from irb
    # Modifies the current object, setting its attributes to a certain version
    # If timestamp is nil, then set to oldest available version
    def set_version(timestamp)
      newest.refresh
      newest.obj_deleted = identity.deleted
      needed_changes = timestamp.nil? ? changes : changes_since(timestamp)
      # Iterate changes, being sure to put them in newest->oldest order first
      needed_changes.reverse.each do |change|
        change.undo_to(newest)
      end
      newest.ct_version = timestamp.nil? ? changes.last.completion_time : timestamp
      identity.deleted = newest.obj_deleted == true ? true : false
      newest
    end
  
    # Returns an array of Changes that occurred *after* the given timestamp
    def changes_since(timestamp)
      raise ArgumentError, "Must pass a Time object, received #{timestamp.class} type" unless timestamp.kind_of? Time
      change_query.filter{completion_time > timestamp}.all
      #Change.filter(:identity_id => identity.id).filter{|c| c.when >= timestamp}.order(:when.desc).all
    end
  
    # Returns an array of Changes made between the given times
    def changes_between(start_time, end_time)
      change_query.filter(:completion_time => (start_time)..(end_time)).all
    end
  
    # Returns the identity associated with this history
    def identity
      newest.ct_identity
    end
  
    def size
      changes.size
    end
  
    def [](index)
      changes[index]
    end
  
    def []=(index, value)
      changes[index] = value
    end
    
    def first
      change_query.first
    end
    
    def last
      change_query.last
    end
  end
end