module ChangeTracker
  class Identity < Sequel::Model
    set_dataset :ct_identities
    plugin :boolean_readers
    def object
      pk = self.class_name.constantize.primary_key
      klass = self.class_name.constantize
      # If deleted, look in deleted dataset, otherwise use normal dataset
      ds = deleted ? klass.deleted : klass
      ds[pk => self.key]
    end
  end
end