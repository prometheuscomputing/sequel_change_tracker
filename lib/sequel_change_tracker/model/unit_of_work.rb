module ChangeTracker
  class UnitOfWork < Sequel::Model
    set_dataset :ct_units_of_work
    #include Singleton
    attr_accessor :changes
    #attr_reader :author
    attr_accessor :failure
  
    def after_initialize
      super
      @failure = false
      if new?
        @changes = Array.new
      else
        @changes = Change.filter(:unit_of_work_id => pk).all
        # Initialize the histories
        @changes.each do |change| 
          identity = ChangeTracker::Identity[change.identity_id]
          change.targets = [identity.object]
        end
      end
    end
    
    # Which objects are directly and explicitly modified by this UOW.
    # This excludes objects modified by ChangeAssociation, unless they store the foreign key.
    # This also excludes objects whose positions within an association are modified by a ChangeAssociation.
    def objects
      @changes.collect {|change| change.targets }.flatten.uniq
    end
    
    def creation_changes
      @changes.select {|change| change.is_a?(ChangeLifecycle) && change.semantics == 'Create'}
    end
    
    def created_objects
      creation_changes.collect {|creation_change| creation_change.target }
    end
    
    def existing_objects
      objects.select{|obj| !obj.new? } - created_objects
    end
    
    def problems
      problems = []
      changes.each do |c|
        problems += c.problems
      end
      problems
    end
  
    # Check whether objects that are going to be modified (or compositions
    # belonging to those objects) have changed
    def has_basis_changed
      existing_objects.any? do |read_object| # Base object as seen by user when editing began
        
        # Current version in the DB
        current_version = read_object.ct_history.last_version
        return false unless current_version
        # Calculate whether or not the basis has changed.
        # Avoid calculating ct_version, since readtime is sufficient
        basis_has_changed = read_object.ct_version_calculated? ? (current_version != read_object.ct_version) : (current_version > read_object.ct_readtime)
          
        if basis_has_changed
          puts "Basis has changed!!!"
          if read_object.ct_version_calculated?
            puts  "current_version: #{current_version} == read_object.ct_version :#{read_object.ct_version}"
          else
            puts  "current_version: #{current_version} == read_object.ct_readtime :#{read_object.ct_readtime}"
          end
          puts "Read Object"
          puts " - object: #{read_object.inspect}"
          if read_object.ct_version_calculated?
            puts " - version: #{read_object.ct_version.inspect} : #{read_object.ct_version.nsec if read_object.ct_version}"
          else
            puts " - readtime: #{read_object.ct_readtime.inspect} : #{read_object.ct_readtime.nsec if read_object.ct_readtime}"
          end
          puts "current (DB)"
          # NOTE: if this is nil, then read_object really wasn't an existing object. It was a created object whose
          #       unit of work was never committed and whose creation change was subsequently erased when a new
          #       unit of work was started
          puts " - version: #{current_version.inspect} : #{current_version.nsec if current_version}"
        end

        basis_has_changed
      end
    end
    
    # Apply the changes contained within this UnitOfWork to the database
    # Returns a boolean
    def apply
      # starting transaction. Transaction calls synchronize internally
      self.class.db.transaction do # Begin transaction
        failure = self.apply_contained_changes
      end # End transaction
      # transaction finished
      failure
    end # End apply
    
    def apply_contained_changes
      save unless ::ChangeTracker.extra_limited_mode # Give the uow an ID
      time = Time.now
      # Using Time.at to remove any precision beyond milliseconds
      self.submission_time = Time.at(time.to_i, time.usec)

      change_number = 0
      @changes.each do |change|
        # case
        # when change.class == ChangeLifecycle
        #   puts "#{change.semantics} -- #{change.target.class}[#{change.target.id}]"
        # when change.class == ChangeAssociation
        #   puts "#{change.semantics} -- #{change.association_operation.model.class}[#{change.association_operation.model.id}] FROM/TO #{change.association_operation.second_model.class}[#{change.association_operation.second_model.id}]"
        # else
        #   puts "#{change.inspect}"
        # end
        change.unit_of_work_id = pk
        change.position = change_number
        # Allow for complex change types, which make more than one simple change
        # by accepting number of completed changes as return from change.do_to()
        # Applying change
        num_changes = change.do
        change_number += num_changes
      end
      
      time = Time.now # TODO: replace this with actual completion time set from DB
      # Using Time.at to remove any precision beyond milliseconds
      self.completion_time = Time.at(time.to_i, time.usec)
      # saving...
      save unless ::ChangeTracker.extra_limited_mode
      
      # On successful application, update local objects' versions
      objects.each do |object|
        object.ct_version = self.completion_time # version is not a persisted value, no need to save
        # Also update readtime since they are now synced with the DB
        object.ct_readtime = self.completion_time
      end
      
      # TODO: should probably handle problems within individual changes
      @changes.each do |change|
        if change.problems.any?
          # FAILURE! Rolling back transaction...
          @failure = true
          # Rollback all changes in this transaction
          raise(Sequel::Rollback) 
        end
      end
      @failure
    end
    
    # Options: 
    #  :lookup_old_associations - By default to_s looks up old associations from DB (Applies only to ChangeAssociation changes)
    def to_s(options = {:lookup_old_associations => true})
      str = []
      str << "UnitOfWork:"
      str << "  author=>#{author}"
      str << "  basis_readtime=>#{basis_readtime}"
      str << " Pending Changes ".center(50, "_")
      changes.each do |change|
        str << (change.to_s(options) << "\n")
      end
      str << "_" * 50
      str.join("\n")
    end
  end
    
end
