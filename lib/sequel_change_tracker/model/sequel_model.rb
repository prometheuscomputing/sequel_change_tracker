# Modifications to Sequel::Model

module Sequel
  class Model
    module InstanceMethods
      def before_change_tracker_destroy; end
      def after_change_tracker_destroy; end
      def before_change_tracker_save; end
      def before_change_tracker_create; end
      def after_change_tracker_create; end
      def after_change_tracker_save; end
    end
  end
end