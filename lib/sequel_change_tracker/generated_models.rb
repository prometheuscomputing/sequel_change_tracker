module ChangeTracker
  
  # Define a mapping between existing module namespaces and generated module namespaces
  def self.module_mappings
    [
      {:old_name => 'Sequel::Plugins::ChangeTracker::ClassMethods', :new_name => 'ChangeTrackerClassMethods', :type => :class},
      {:old_name => 'Sequel::Plugins::ChangeTracker::InstanceMethods', :new_name => 'ChangeTrackerInstanceMethods', :type => :instance},
      {:old_name => 'Sequel::Plugins::ChangeTracker::SCTClassMethods', :new_name => 'ChangeTrackerClassMethods', :type => :translate_only},
      {:old_name => 'Sequel::Plugins::ChangeTracker::SCTInstanceMethods', :new_name => 'ChangeTrackerInstanceMethods', :type => :translate_only},
      {:old_name => 'Sequel::Plugins::ChangeTracker', :new_name => 'ChangeTracker', :type => :translate_only}
    ]
  end
  
  # Ruby classes needed by SCT functions
  def self.explicit_helper_classes
    d = []
    models = %w(extensions change deleted_item history identity problem unit_of_work errors sequel_model)
    models.each do |model|
      d << File.read("sequel_change_tracker/model/#{model}.rb".find_on_load_path)
    end
    # Overwrite ChangeTracker.define_models to prevent double loading of model classes (from gem!)
    d << "module ChangeTracker; def self.define_models; end; end"
    d << File.read('sequel_change_tracker/controller/cache.rb'.find_on_load_path)
    d << File.read('sequel_change_tracker/controller/session.rb'.find_on_load_path)
    d << File.read('sequel_change_tracker/controller/controller.rb'.find_on_load_path)
    d << File.read('sequel_change_tracker/sql/schemas.rb'.find_on_load_path)
    d << File.read('sequel_change_tracker/sql/migration.rb'.find_on_load_path)
    # Setup ChangeTracker db and define schemas
    d << "ChangeTracker.setup"
    d << apply_method
    d.join("\n\n")
  end
  
  def self.apply_method
    d = ["module ChangeTracker"]
    # d << "  extend self" if new_module_info[:type] == :class
    
    plugin_class_instance = class_instance_for(Sequel::Plugins::ChangeTracker)
    method = plugin_class_instance.instance_method(:apply)
    d << method.source.reset_indentation.indent
    d << "end"
    d.join("\n\n")
  end
  
  def self.apply_code
    # def self.apply(model, username_location = nil, uow_location = nil)
    "ChangeTracker.apply(self)"
  end
  
  def self.class_instance_for(klass)
    metaclass = class << klass; self; end
    metaclass
  end
end