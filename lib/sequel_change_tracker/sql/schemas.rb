module ChangeTracker
  def self.create_tables
    ChangeTracker.db.create_table? :ct_identities do
      primary_key :id
      Fixnum :key
      String :class_name
      TrueClass :deleted, :default => false
      index [:key, :class_name]
    end

    ChangeTracker.db.create_table? :ct_units_of_work do
      primary_key :id
      Time :submission_time
      Time :completion_time
      Time :basis_readtime
      String :author
    end

    ChangeTracker.db.create_table? :ct_changes do
      primary_key :id
      #String :author # Could be an id depending on how users are tracked
      String :change_type # Used by single_table_inheritance plugin to store Change type
      String :attribute_name
      File :old_value
      File :new_value
      String :semantics
      String :key
      String :new_key
      Fixnum :position
      Fixnum :identity_id # This is the target's identity
      Fixnum :unit_of_work_id
      # -- ChangeAssociation persistance cols --
      String :getter
      Fixnum :model_identity_id
      Fixnum :second_model_identity_id
      # ----------------------------------------
      index [:identity_id]
      index [:model_identity_id]
      index [:second_model_identity_id]
    end

    ChangeTracker.db.create_table? :ct_problems do
      primary_key :id
      Fixnum :change_id
      String :description
      Text :stack_dump
    end
  end
end