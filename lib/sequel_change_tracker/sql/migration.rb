module ChangeTracker
  # Automatically update SCT tables in database to add in columns needed for newer versions of SCT
  def self.migrate_db?(database = nil)
    database ||= ChangeTracker.db
    # Add columns needed for ChangeAssociation persistence
    # This is required if a database was created with SCT versions < 2.8.0
    unless database[:ct_changes].columns.include?(:getter)
      database.add_column(:ct_changes, :getter, String)
      database.add_column(:ct_changes, :model_identity_id, Fixnum)
      database.add_column(:ct_changes, :second_model_identity_id, Fixnum)
    end
    # Add missing indices for model_identity_id and second_model_identity_id
    unless database.indexes(:ct_changes).key?(:ct_changes_model_identity_id_index)
      database.add_index(:ct_changes, :model_identity_id)
      database.add_index(:ct_changes, :second_model_identity_id)
    end
  end
end