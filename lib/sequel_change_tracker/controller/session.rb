module ChangeTracker
  class Session
    attr_accessor :unit_of_work
    attr_reader :browse_time
    # Cache object that handles caching during time browsing
    attr_accessor :cache
  
    def self.get_current
      ChangeTracker.thread_session_hash[:ct_session] ||= Session.new
    end
  
    def initialize
      @unit_of_work = nil
      @browse_time = nil
      @cache = Cache.new
    end
  
    def browse_time=(browse_time)
      return browse_time if @browse_time == browse_time
      # Empty cache
      @cache.clear
      # Set the new browse time
      @browse_time = browse_time
    end
    
    def start
      return true if ChangeTracker.disabled
      # Check if model has been loaded
      unless defined?(ChangeTracker::UnitOfWork)
        raise "Error: ChangeTracker was asked to start without being applied to any models. Did you forget to apply the <<change_tracked>> stereotype to your package before generating?"
      end
    
      # Create a new uow; set its basis_readtime to now
      basis_readtime = Time.now
      # Set unit_of_work to new UOW. This causes any previous UOW to be removed.
      @unit_of_work = ChangeTracker::UnitOfWork.new(
        :author => ChangeTracker.author, 
        :basis_readtime => Time.at(basis_readtime.to_i, basis_readtime.usec))
    end
  
    # Checks if a Unit of Work has been started
    def started?
      !@unit_of_work.nil?
    end
  
    # Cancel the current uow
    def cancel
      return true if ChangeTracker.disabled
      @unit_of_work = nil
      true
    end
  
    # Clears any changes from a started uow without cancelling
    def clear_changes
      raise NotStartedError, "No unit of work started." unless @unit_of_work
      @unit_of_work.changes.clear
    end
  
    # Commits all changes in the current session's unit of work to the DB
    # If a block is passed, a new unit_of_work is started and the block is run
    # before the commit occurs.
    #
    # Return values:
    #  true - successful commit
    #  false - no unit of work started, or no changes. No commit made
    #  <#UnitOfWork> - Problem occurred. No commit made
    def commit &block
      if ChangeTracker.disabled
        if block_given?
          yield
        end
        return true
      end

      if block_given?
        ChangeTracker.start
        yield
      else
        raise NotStartedError, "No unit of work started." unless @unit_of_work
      end

      if ::ChangeTracker.mutex_enabled?
        ::ChangeTracker.mutex.synchronize { _commit }
      else
        _commit
      end
    end
  
    # Commit implementation
    def _commit
      basis_changed = false
  
      if @unit_of_work.changes.empty? # No changes
        # No changes made
        @unit_of_work = nil
        false
      else # Changes exist in current unit of work
        # checking basis...
        basis_changed = @unit_of_work.has_basis_changed
    
        if ! basis_changed
          # basis was good
          # Get success/failure value from unit of work application
          failure = @unit_of_work.apply
        else # Objects referenced by uow have been changed
          #TODO: put this in tests
          # Prompt user with changes
          # Basis has changed! Cancelling uow
          failure = @unit_of_work
        end
        old_uow = @unit_of_work # Save a reference to the uow to return in case of failure
        @unit_of_work = nil
        if failure
          old_uow.problems.each do |problem|
            puts "Description: #{problem.description}"
            puts "Dump: #{problem.stack_dump}" if problem.stack_dump
          end
      
          if basis_changed # failure was due to a basis change
            # TODO: write spec test for this!
            raise BasisChangedError, "Transaction failed! Basis was changed."
          else
            raise ApplyError, "Transaction failed! Failed to apply changes."
          end
        end
        failure ? old_uow : true
      end # End if changes
    end # End commit

    # Simplistic return for Java until we work out how to return failure information correctly from commit.
    def jcommit
      result = self.commit
      result.is_a?(TrueClass) || result.is_a?(FalseClass)
    end
    
    def ignore_browse_time &block
      original_bt = self.browse_time
      self.browse_time = nil
      value = yield
      self.browse_time = original_bt
      value
    end
  end
end