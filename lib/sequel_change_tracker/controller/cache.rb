module ChangeTracker
  class Cache
    attr_accessor :cache
  
  
    def get_cache(klass, id)
      klass_cache = @cache[klass.to_s]
      klass_cache ? klass_cache[id] : nil
    end
  
    def get_all_cache(klass)
      cache = @cache[klass.to_s]
      # Is the cache a complete set for the specified time?
      cache_complete = cache && cache[:complete]
      if cache_complete
        klass_cache = @cache[klass.to_s].values
        # Remove the ':complete => true' from the returned values
        klass_cache.delete(true)
        klass_cache
      else
        nil
      end
    end
  
    def set_cache(object, klass = nil)
      raise "When setting cache, object #{object.inspect} could not be cached" + 
        " since it does not respond to #id" unless object.respond_to?(:id)
      klass ||= object.class
      @cache[klass.to_s] ||= {}
      @cache[klass.to_s][object.id] = object
    end
  
    def set_all_cache(objects, klass = nil)
      if objects.any?
        # Set klass from first object in set unless already defined
        klass ||= objects.first.class
      else
        # No objects, so return unless we know what klass this is for
        return unless klass
      end
      @cache[klass.to_s] ||= {}
      objects.each do |object|
        @cache[klass.to_s][object.id] = object
      end
      # Mark the cache as complete
      @cache[klass.to_s][:complete] = true
    end
  
    def clear
      @cache = {}
    end

    # This method attempts to guess the klass of the object(s) passed
    # This would cause empty arrays to not be cached
    # def <<(obj)
    #   case obj
    #   when Array
    #     set_all_cache obj
    #   when Sequel::Model
    #     set_cache obj
    #   else
    #     raise ArgumentError, "Cannot cache object of type #{obj.class.to_s}"
    #   end
    # end
  
    def [](klass, id = nil)
      id ? get_cache(klass, id) : get_all_cache(klass)
    end
  
    def []=(klass, id = nil, obj)
      id ? set_cache(obj, klass) : set_all_cache(obj, klass)
    end
  end
end