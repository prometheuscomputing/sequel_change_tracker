require 'forwardable'

module ChangeTracker
  class << self
    # Database used by SCT persisted classes
    attr_accessor :db
    # Whether or not to use a mutex for all SCT operations
    attr_accessor :mutex_enabled
    # The mutex to use for all SCT operations if enabled
    attr_accessor :mutex
    # Whether or not SCT has been set up
    attr_accessor :setup
    # The key in the session hash that points to the value to use for a unit of work's author (defaults to :username)
    attr_accessor :author_key
    # A user defined session_hash, which is used if not using the Ramaze session hash
    attr_accessor :session_hash
  end
  
  def self.setup(database = nil)
    ::ChangeTracker.db = database || Sequel::Model.db
    ::ChangeTracker.mutex_enabled = (defined?(Sequel::SQLite::Database) && ::ChangeTracker.db.is_a?(Sequel::SQLite::Database)) if ::ChangeTracker.mutex_enabled.nil?
    ::ChangeTracker.mutex ||= Monitor.new
    ::ChangeTracker.create_tables
    ::ChangeTracker.migrate_db?
    ::ChangeTracker.define_models
    # Cache columns for SCT models (required for generated models?)
    ChangeTracker::ChangeAttribute.columns
    ChangeTracker::ChangeLifecycle.columns
    ChangeTracker::ChangeAssociation.columns
    ChangeTracker::Identity.columns
    ChangeTracker::UnitOfWork.columns
    ChangeTracker::Problem.columns
    @setup = true
  end
  
  def self.delete_all_history
    Sequel::Model.children.each do |klass|
      next unless klass.plugins.include?(Sequel::Plugins::ChangeTracker)
      klass.deleted.delete if klass.deleted_table_exists?
    end
    ChangeTracker::Change.delete
    ChangeTracker::Identity.delete
    ChangeTracker::UnitOfWork.delete
    ChangeTracker::Problem.delete
  end

  def self.setup?
    @setup == true
  end
  
  def self.mutex_enabled?
    @mutex_enabled
  end
  
  # Retrieve the key used to retrieve the author from the user session hash
  def self.author_key
    @author_key || :username
  end
  
  def self.get_ramaze_session
    return nil unless defined?(Ramaze)
    return nil unless defined?(Innate)
    require 'rack'
    require 'innate'
    extend Innate::Trinity
    session
  end
  
  # Returns the user-specific session hash.
  # This hash is not thread-specific, but rather contains thread specific session hashes within it
  def self.user_session_hash
    # Return defined session_hash or ramaze session
    hash = get_ramaze_session || ChangeTracker.session_hash
    unless hash
      puts "ChangeTracker: Ramaze session could not be loaded. Setting up new hash to use for session."
      hash = self.session_hash = {}
    end
    hash
  end

  def self.disabled
    @disabled == true
  end

  def self.disabled=(value)
    @disabled = !!value
  end
  
  def self.limited_mode
    @limited_mode == true
  end

  def self.limited_mode=(value)
    @limited_mode = !!value
  end
  
  def self.extra_limited_mode
    @extra_limited_mode == true
  end
  
  def self.extra_limited_mode=(value)
    @extra_limited_mode = !!value
    self.limited_mode = value
  end
  
  def self.mode
    return "disabled"      if disabled
    return "extra_limited" if extra_limited_mode
    return "limited"       if limited_mode
    "normal"
  end
  
  # Begin backwards compatibility methods
  # ------------------------------------- 
  # Returns the thread-specific session hash
  def self.thread_session_hash
    hash = user_session_hash
    # Return thread-specific hash
    sct_hash = hash[:sequel_change_tracker] ||= {}
    sct_hash[Thread.current.object_id] ||= {}
  end
  
  # Reset all SCT sessions by deleting all Thread-specific sessions in storage hash.
  def self.reset_all
    self.user_session_hash.delete(:sequel_change_tracker)
  end
  
  # Retrieve the author from the user session hash, using the author key specified
  def self.author
    user_session_hash[self.author_key]
  end
  
  # Retrieve the current session
  def self.current_session
    Session.get_current
  end

  # Forward all the specified methods to the current session
  class << self
    extend Forwardable
    def_delegators :current_session, :start, :started?, :cancel, :clear_changes, :commit, :jcommit, :browse_time, :browse_time=, :unit_of_work, :unit_of_work=, :ignore_browse_time # , :limited_mode, :limited_mode=, :extra_limited_mode, :extra_limited_mode= # , :disabled, :disabled=
  end
end