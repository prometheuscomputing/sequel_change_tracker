module ChangeTracker
  
  # For more information about meta_info.rb, please see project Foundation, lib/Foundation/meta_info.rb

  # Required
  GEM_NAME = "sequel_change_tracker"
  VERSION  = '2.16.10'
  AUTHORS  = ["Sam Dana", "Ben Dana"]
  SUMMARY = %q{Plugin to Ruby Sequel that allows long transactions and history tracking}
  
  # Optional
  EMAILS      = ["s.dana@prometheuscomputing.com", "b.dana@prometheuscomputing.com"]
  HOMEPAGE    = nil
  DESCRIPTION = %q{Use 'plugin :change_tracker' to use this plugin in your Sequel Models. 
    Once activated, it will track changes to the Models. Transactions are started
    via ChangeTracker.start and committed via ChangeTracker.commit}

  LANGUAGE         = :ruby
  LANGUAGE_VERSION = '>= 2.3'
  RUNTIME_VERSIONS = {
    :mri   => '>= 2.3',
    :jruby => '>= 9.1'
  }
  TYPE     = :library
  LAUNCHER = nil
  DEPENDENCIES_RUBY = {
    :sequel                       => '4.41',
    :sequel_specific_associations => ['~> 11.0']
  }
   
  DEPENDENCIES_MRI   = { }
  DEPENDENCIES_JRUBY = { }
  DEVELOPMENT_DEPENDENCIES_RUBY = {
    # test-unit is reccomended but not required (color codes Test::Unit test results)
    :rspec       => '~> 3.0',
    :car_example => '~> 1.21',
    :gui_director => nil,
    :car_example => nil
  }
  DEVELOPMENT_DEPENDENCIES_MRI   = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  
  # An Array of strings that YARD will interpret as regular expressions of files to be excluded.
  YARD_EXCLUDE = ['lib/.*/templates/.*']
  
end