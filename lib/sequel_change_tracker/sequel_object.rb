module Sequel
  module Plugins
    module ChangeTracker
      def self.load_schemas_and_models
        require 'sequel_change_tracker/sql/schemas'
        require 'sequel_change_tracker/model/init'
      end
      
      # Apply the change tracker plugin to the passed model
      def self.apply(model)
        # Check to make sure change tracker has not already been applied (This can happen via inheritance, and
        #   will cause errors if applied twice)
        if model.respond_to?(:change_tracked?) && model.change_tracked?
          # Ignoring attempt to apply ChangeTracker plugin to already tracked model
          return
        end

        # Allow for the plugin being disabled. 
        # This requires sneaky module name manipulation since Sequel will include ClassMethods and InstanceMethods if they exist.
        session_hash_defined = ::ChangeTracker.session_hash || ::ChangeTracker.get_ramaze_session
        unless session_hash_defined && ::ChangeTracker.disabled
          # Not disabled. Make SCTClassMethods and SCTInstanceMethods available as ClassMethods and InstanceMethods
          Sequel::Plugins::ChangeTracker.const_set(:ClassMethods, Sequel::Plugins::ChangeTracker::SCTClassMethods) unless Sequel::Plugins::ChangeTracker.const_defined?(:ClassMethods)
          Sequel::Plugins::ChangeTracker.const_set(:InstanceMethods, Sequel::Plugins::ChangeTracker::SCTInstanceMethods) unless Sequel::Plugins::ChangeTracker.const_defined?(:InstanceMethods)
        else
          # Disabled. Make sure any before/after change tracker save hooks still run by wrapping save method functionality
          model.send(:alias_method, :sequel_save, :save)
          model.send(:define_method, :save) {
            before_change_tracker_save
            before_change_tracker_create if new?
            result = sequel_save
            after_change_tracker_create if new?
            after_change_tracker_save
            result
          }
          return
        end
        ::ChangeTracker.setup(model.db) unless ::ChangeTracker.setup?
        Sequel::Plugins::ChangeTracker::InstanceMethods.send(:include, ::ChangeTracker)
        model.send(:alias_method, :save_original, :save)
        model.send(:alias_method, :destroy_original, :destroy)
        
        # Mark the model as change tracked
        class << model
          def change_tracked?; true; end
        end
        
        # This method has to be in a 'define_method' since it must override the complex_attribute save override.
        # This is because complex_attribute! uses define_method and it would otherwise take priority over a method
        #   defined in an included module (typically this method would be in InstanceMethods)
        model.send(:define_method, :destroy) do |*columns|
          return destroy_original if ::ChangeTracker.disabled
          raise ::ChangeTracker::NotStartedError, "You must start a unit of work first!" unless ct_unit_of_work
          return nil if self.respond_to?(:before_change_tracker_destroy) && before_change_tracker_destroy == false
          # Create change object
          change = ::ChangeTracker::ChangeLifecycle.build(self, :Destroy)
          ct_unit_of_work.changes << change
          self.respond_to?(:after_change_tracker_destroy) && after_change_tracker_destroy
          self
        end

        # This method has to be in a 'define_method' since it must override the complex_attribute save override.
        # This is because complex_attribute! uses define_method and it would otherwise take priority over a method
        #   defined in an included module (typically this method would be in InstanceMethods)
        model.send(:define_method, :save) do |*columns|
          return save_original if ::ChangeTracker.disabled
          # Can't save complex attributes
          return self if self.class.respond_to?(:complex_attribute?) && self.class.complex_attribute?
          # Saving
          raise ::ChangeTracker::NotStartedError, "You must start a unit of work first!" unless ct_unit_of_work

          # Michael's experiment
          # make sure each hook is only run once
          unless @first_pass_started
            @first_pass_started = true
            # puts "Begin #{self.class}-#{self.object_id}.ct_save"

            # Call before_change_tracker_save hook. Return nil if value returned by hook is false (same behavior as Sequel hooks)
            # puts "  @before_save_executed #{@before_save_executed.inspect}"
            unless @before_save_executed
              # puts "    from #{self.class}.ct_save  BEFORE SAVE"
              @before_save_executed = true
              raise_hook_failure(:before_change_tracker_save) if before_change_tracker_save == false
            end

            # Call before_change_tracker_create hook.
            # puts "  @before_create_executed #{@before_create_executed.inspect}"
            if !@before_create_executed && new?
              # puts "    from #{self.class}.ct_save  BEFORE CREATE"
              @before_create_executed = true
              raise_hook_failure(:before_change_tracker_create) if before_change_tracker_create == false
            end
          
            # Parse all changes to the object (except destroy) and save change objects to the unit of work
            ct_unit_of_work.changes += parse_changes
            # Call after_change_tracker_create hook.
            # puts "  @after_create_executed #{@after_create_executed.inspect}"
            if !@after_create_executed && new?
              # puts "    from #{self.class}.ct_save  AFTER CREATE"
              @after_create_executed = true
              after_change_tracker_create
            end
          
            # Call after_change_tracker_save hook.
            # puts "  @after_save_executed #{@after_save_executed.inspect}"
            unless @after_save_executed
              # puts "    from #{self.class}.ct_save  AFTER SAVE"
              @after_save_executed = true
              after_change_tracker_save
            end
            # puts "End #{self.class}-#{self.object_id}.ct_save"; puts
            @before_save_executed   = nil
            @before_create_executed = nil
            @after_create_executed  = nil
            @after_save_executed    = nil
            @first_pass_started     = nil
            self
          else
            # Parse all changes to the object (except destroy) and save change objects to the unit of work
            ct_unit_of_work.changes += parse_changes
            self
          end
        end
      end #self.apply
      
      module SCTClassMethods
        def session
          ::ChangeTracker.current_session
        end
        
        def browse_time
          session.browse_time
        end
        
        def cache
          session.cache
        end

        # Return all objects in the table
        # If a browse_time for this session is set, this method will
        # return objects existant at that time with the proper state set
        def all(&block)
          #check_cache
          if browse_time
            # Get a cache of the table if one exists
            valid_cache = cache[self]
            if valid_cache
              # Return objects from cache that were not deleted at that point in time
              valid_cache.reject{|m| m.deleted? }
            else
              # Get all objects (existant and deleted)
              all_objs = all_historical #(&block)
              # Set the state of each object to the specified time
              all_at_time = all_objs.collect {|m| 
                              # Get objects from cache or set state from history
                              cache[self, m.id] ||
                              m.ct_history.set_version(browse_time) 
                          }
              # Add the set of all objects to the cache (includes deleted objects)
              cache[self] = all_at_time
              # Return objects that were not deleted at that point in time
              all_at_time.reject{|m| m.deleted? }
            end
          else
            super
          end
        end
        
        # Return all objects existant and deleted
        def all_historical(&block)
          #TODO: I don't think this is including deleted items from other tables
          ::ChangeTracker.ignore_browse_time { all(&block) + deleted.all(&block) }
        end
        
        # Return an object from given arguments
        # If a browse_time for this session is set, this method will
        # return the object if it was existant at that time with the proper state set
        # NOTE: time_browsing is incompatible with the filter command, so commands
        #       such as Klass[:name => 'fred'] will not work. (Only :id is a valid selector)
        #
        # Usage:
        # Klass[:id => 3] --OR-- Klass[3]
        # => #<Klass @values={:id=>3, ...}>
        #
        def [](*args)
          # If a browse time is set, return state of requested object at specified time
          if browse_time
            args = args.first if (args.size == 1)
            # Get ID from args
            id = args.is_a?(Hash) ? args[:id] : args
            # Look in cache for object
            valid_cache = cache[self, id]
            if valid_cache
              # Cache was good, return cache as object
              object = valid_cache
            else
              # Look in primary table for object
              object = args.is_a?(Hash) ? dataset[args] : primary_key_lookup(args)
              if !object # Couldn't find object in primary table
                # Look in deleted table for object
                object = args.is_a?(Hash) ? deleted[args] : deleted_pk_lookup(args)
              end
              if object # If object was found in either table
                # Set the state of the object to the specified time
                object.ct_history.set_version(browse_time)
                # Add the object to the cache (even if deleted? is true)
                cache[self, id] = object
              end
            end
            # Return the object if one was found and it was not deleted
            (!object || object.deleted?) ? nil : object
          else
            # No browse_time set, use original brackets method
            super
          end
        end
        
        def deleted_pk_lookup(pk)
          deleted[primary_key_hash(pk)]
        end
        
        def set_deleted_dataset(ds = nil)
          @deleted = ds || begin
            del_dataset = db[deleted_table_name]
            del_dataset.row_proc = Proc.new{|r| load(r)}
            @dataset_method_modules.each{|m| del_dataset.extend(m)} if @dataset_method_modules
            @dataset_methods.each{|meth, block| del_dataset.meta_def(meth, &block)} if @dataset_methods
            del_dataset.model = self if del_dataset.respond_to?(:model=)
            del_dataset
          end
        end

        def deleted
          @deleted || set_deleted_dataset
        end
        
        # Override instance_dataset reader to accept an optional is_deleted parameter
        def instance_dataset(is_deleted = false)
          if is_deleted
            @deleted_instance_dataset ||= deleted.limit(1).naked
          else
            @instance_dataset ||= dataset.limit(1).naked
          end
        end
        
        def deleted_instance_dataset
          instance_dataset(true)
        end
        
        # Returns the name of the table to store deleted items
        def deleted_table_name
          "#{table_name.to_s}_deleted".to_sym
        end
        
        # Check whether or not the deleted items table exists
        def deleted_table_exists?
          self.db.table_exists?(deleted_table_name)
        end

        # Copy the schema of the model's table, and create a new table for deleted items
        def create_deleted_table
          schema = db.schema(table_name)
          db.create_table deleted_table_name do
            schema.each do |c|
              column(c[0], nil, c[1])
            end
          end
        end
        
        # Drop the deleted items table
        def drop_deleted_table
          db.drop_table(deleted_table_name)
        end
      end # End ClassMethods
      ClassMethods = SCTClassMethods
      
      module SCTInstanceMethods
        attr_writer :ct_identity
        attr_writer :ct_history
        attr_writer :ct_version
        attr_accessor :ct_readtime
        # Used by change to tell whether this object is deleted at a point in time
        attr_accessor :obj_deleted
        attr_accessor :original_values
        
        def ct_identity
          return nil if self.class.respond_to?(:complex_attribute?) && self.class.complex_attribute?
          @ct_identity ||= 
            begin
              identity_info = {:key => pk, :class_name => self.class.to_s, :deleted => false}
              if ::ChangeTracker.mutex_enabled?
                ::ChangeTracker.mutex.synchronize{ Identity.create(identity_info) }
              else
                Identity.create(identity_info)
              end
            end
        end
        
        def ct_history
          @ct_history ||= History.new(self)
        end
        
        def ct_version(opts = {})
          @ct_version ||= begin 
            puts "Warning: Costly calculation of ct_version"
            ct_history.version_time_before(@ct_readtime)
          end
        end
        
        def ct_version_calculated?
          !@ct_version.nil?
        end
        
        # TODO: could move all this into initialize after super ( no need for after_init alias )
        # TODO: This should be changed to before_initialize (doesn't exist) so as to allow associations to be set via #create and #new
        def after_initialize
          super
          
          read_time = Time.now
          @ct_readtime = Time.at(read_time.to_i, read_time.usec)
          
          if new?
            @original_values = {}
            @changed_columns = @values.keys
            if ct_unit_of_work
              @ct_version = ct_unit_of_work.basis_readtime
            else
              version_time = Time.now
              @ct_version = Time.at(version_time.to_i, version_time.usec)
            end
          else # was not new
            # Save the original values of the object to @original_values hash
            @original_values = @values.clone
            @ct_identity = Identity[:key => self.pk, :class_name => self.class.to_s] || ct_identity
            @ct_version = nil # Not setting due to high performance cost. Will be set if needed
          end
        end

        def refresh
          val = super
          @ct_version = nil # Not setting due to high performance cost. Will be set if needed
          read_time = Time.now
          @ct_readtime = Time.at(read_time.to_i, read_time.usec)
          # Not sure if this is needed here vvv
          @original_values = @values.clone
          val
        end


        # After the object is created, if it was new, set the object's IDENTITY's id and save it
        def after_create
          # Create a new ct_identity for this object
          ct_identity if was_new?
          # assigning ct_identity key
          super
          # FIXME: this solution should be generalized to all timestamp attributes
          # Trim nanosecond precision from automatic created_at timestamp
          self.created_at = Time.at(created_at.to_i, created_at.usec) if respond_to?(:created_at) && created_at
          if was_new?
            @ct_identity.key = pk
            @ct_identity.save
          end
        end

        def before_save
          super
        end
        
        # After the object is saved, copy its values to original values
        def after_save
          super
          # Trim nanosecond precision from automatic updated_at timestamp
          self.updated_at = Time.at(updated_at.to_i, updated_at.usec) if respond_to?(:updated_at) && updated_at
          @original_values = @values.clone
        end

        def parse_changes
          changes = []
          lifecycle_change = check_for_lifecycle_change
          changes << lifecycle_change if lifecycle_change
          # NOTE: no longer parsing attribute changes on Create/Delete
          changes += check_for_attribute_changes unless lifecycle_change
          changes += check_for_association_changes
          changes
        end
          
        
        # Check for a lifecycle creation change (return nil if no change)
        def check_for_lifecycle_change
          if (new? && !ct_created?) || deleted?
            # creating ChangeLifecycle create event
            change = ChangeLifecycle.build(self, :Create)
            
            @ct_created = true
            change
          end
        end
        
        def check_for_attribute_changes
          # the :changed => true option, and only iterating over changed
          # columns. This will prevent storing of changes where old and new 
          # values are the same.
          attribute_changes = []
          changed_columns.each do |column|
            next if !columns.empty? && !columns.include?(column)
            original_value = @original_values[column] if @original_values
            change = ChangeAttribute.build(self, column, original_value, @values[column])
            attribute_changes << change
          end
          reset_changed_columns
          attribute_changes
        end
        
        def reset_changed_columns
          @changed_columns = []
        end
        
        def check_for_association_changes
          # Adding changes corresponding to pending associations
          # This occurs after column changes are processed so that associations take
          # precedence over any changes that were made to individual columns
          association_changes = []
          if self.class.private_instance_methods.include?(:pending_association_operations) || self.respond_to?(:pending_association_operations)
            @pending_association_operations ||= []
            @pending_association_operations.each do |association_operation|
              # creating ChangeAssociation event
              change_association = ChangeAssociation.build(self, association_operation)
              association_changes += change_association
            end
            @pending_association_operations.clear
          end
          association_changes
        end
        
        # Destroy references to the object, but leave it in place.
        # Creates an entry in the DeletedItem table
        def disable
          # calls before_destroy, after_destroy hooks
          begin
            destroy_original
          rescue Sequel::NoExistingObject
            # No existing object. Was likely deleted by a change in this same commit.
          end
          @ct_identity.deleted = true
          @ct_identity.save
          self
        end

        # Saves the values of the domain object before it is destroyed
        def before_destroy(is_actual_destroy = true)
          super(is_actual_destroy)
          if is_actual_destroy
            @saved_values = @values.clone
            # Insert a copy of the record into the deleted items table
            create_deleted_table unless deleted_table_exists?
            # Inserting into deleted_table
            # Delete any existing rows w/ the same id (newer deletions take priority)
            # TODO: could enhance destroy performance by inserting and catching error when id already exists (then filter/delete & insert again)
            db[deleted_table_name].filter(:id => @saved_values[:id]).delete
            db[deleted_table_name].insert(@saved_values)
          end
        end
        
        # un-deletes deleted objects
        def enable
          if !@ct_identity.deleted
            raise InvalidOperation, "attempted to undelete an object that has not been deleted"
          else # Object was deleted
            # Return the row to its original position in the table
            del_dataset = db[deleted_table_name].filter(self.class.primary_key => pk)
            db[self.class.table_name].insert(del_dataset.first)
            del_dataset.delete
            @ct_identity.deleted = false
            @ct_identity.save
            drop_deleted_table if db[deleted_table_name].count == 0
            self
          end
        end

        # Define save_changes_original to correctly call save_original and not the overridden save metho
        def save_changes_original(opts={})
          save_original(opts.merge(:changed=>true)) || false if modified? 
        end

        # This is providing access to information created by Sequel in #_save, which is where all the hooks get applied
        def was_new?
          @was_new
        end

        # Returns whether or not a create event has already been created for this object
        def ct_created?
          @ct_created
        end

        # Convenience accessor for the session's ct_unit_of_work
        def ct_unit_of_work
          self.class.session.unit_of_work
        end

        def deleted?
          @ct_identity.deleted
        end
        
        # Override the default #this method to pass parameter to model.instance_dataset
        def this
          return @this if @this
          raise Error, "No dataset for model #{model}" unless ds = model.instance_dataset(deleted?)

          cond = if ds.joined_dataset?
            qualified_pk_hash
          else
            pk_hash
          end

          @this = use_server(ds.where(cond))
        end
        
        # Provide aliases to ct_* methods for backwards compatibility with older versions of ChangeTracker
        def identity
          puts "Deprecation warning: Sequel::Model#identity has been deprecated. Use #ct_identity instead"
          ct_identity
        end
        def history
          puts "Deprecation warning: Sequel::Model#history has been deprecated. Use #ct_history instead"
          ct_history
        end
        def version
          puts "Deprecation warning: Sequel::Model#version has been deprecated. Use #ct_version instead"
          ct_version
        end
        def unit_of_work
          puts "Deprecation warning: Sequel::Model#unit_of_work has been deprecated. Use #ct_unit_of_work instead"
          ct_unit_of_work
        end
        # End backwards compatibility methods

        def deleted_table_name; self.class.deleted_table_name; end
        def deleted_table_exists?; self.class.deleted_table_exists?; end
        def create_deleted_table; self.class.create_deleted_table; end
        def drop_deleted_table; self.class.drop_deleted_table; end
      end # End InstanceMethods
      

    end # End ChangeTracker
  end # End Plugins
end # End Sequel
