puts "sequel_change_tracker/commit_wrapper and the #commit method are deprecated and will be removed in SCT v2.0.0. Remove commit_wrapper require and use ChangeTracker.commit {...} instead."

# Wraps a block in a transaction. Returns the result of block execution.
def commit(session_hash={})
  ChangeTracker.session_hash=session_hash
  ChangeTracker.start
  result = nil
  begin 
    result = yield
  rescue => err 
    ChangeTracker.cancel
    stk = err.backtrace.join("\n\t") 
    puts "Transaction cancelled: #{err.message} \n\t#{stk}"
    raise err
  end 
  ChangeTracker.commit
  result
end