Feature: Change management
	As an Analyst
	I want to ensure that use cases continuously improve, and never regress
	So that we have quality requirements on which to base further development
	
=begin
Originally I intended to use "optimistic concurrency control" based on object time stamps, with a little
extra intelligence to avoid changing the timestamps on objects that don't change (even if they are involved
in a unit of work). 

I now think the usual approaches to concurrency control are based on a faulty assumption: that each
user must have their own view of the data. The usually stated reason is to give the user a
self consistent view of the repository. That's a sound idea when automated processes need to modify the data,
but it doesn't give human users enough credit for their problem solving skills. Ultimately a person will probably
need to make a decision in such cases anyway.  Why not have them share a single view, with changes made by other
users plainly identified?  This would preemptively eliminate conflicts. This would not work very well if many 
users are modifying the same data, but that's not the case here (and often not the case elsewhere).

I've therefore described two versions, static and dynamic. The dynamic version is preferable.
If the dynamic version proves too difficult to implement, then the static version would be satisfactory.
=end
	
Scenario Outline: Unit of work
	Given <what> and <what else> exist <where>
	And I acquire a view of <what> and <what else>
	And nobody else changes <what> or <what else> between the time I acquire my view, and the time I commit
	When I commit a modification to <what> together with a modification to <what else> at time t
	Then the system knows that <what> and <what else> were modified during the same unit of work
	And the unit of work may have been bundled with other units of work into a single transaction for the sake of efficiency

	Examples:
	  | what                    | what else     | where                                                                 |
	  | description             | category      | UseCase "Consumer Portal" scenario "Receive Real Time Pricing Update" |
	  | ontology.uml_model_URL  | name          | System "Building Automation System"                                   |
	
Scenario Outline: Conflict resolution (static) # incompatible with dynamic: implement one or the other
	Given <what> and <what else> exist <where>
	And I acquire a view of <what> and <what else>
	When <somebody else> commits a change to <what> or <what else>
	Then I am warned about the change(s) made by <somebody else>
	And the changes are explained to me
	And it's up to me, perhaps with the cooperation of the other user(s), to ensure the data is correct if & when I commit
	And my unit of work is checked for unapproved conflicts (resulting from latency in notifying me of the changes)

	Examples:
	  | what                    | what else     | where                                                                 | somebody else  | 
	  | description             | category      | UseCase "Consumer Portal" scenario "Receive Real Time Pricing Update" | Bob            |
	  | ontology.uml_model_URL  | name          | System "Building Automation System"                                   | Alice          |

Scenario Outline: Conflict resolution (dynamic) # incompatible with static: implement one or the other
	Given <what> and <what else> exist <where>
	And I acquire a shared view of <what> and <what else>
	When <somebody else> makes a change to <what> or <what else>
	# The following is not limited to changes by *one* other party, could be several
	Then I can see <somebody else>'s change(s) in real time, even before he/she commits them # for example, in a diff-like display. 
	And the changes are called to my attention # for example, by highlighting
	And I can readily determine who made the changes # for example, by highlight color, with table showing colors used by different parties
	And it's up to everyone who commits to ensure the shared view is correct
	And commits are checked to ensure the committer had enough time to see all the changes before deciding to commit

	Examples:
	  | what                    | what else     | where                                                                 | somebody else  | 
	  | description             | category      | UseCase "Consumer Portal" scenario "Receive Real Time Pricing Update" | Bob            |
	  | ontology.uml_model_URL  | name          | System "Building Automation System"                                   | Alice          |


Scenario Outline: Track changes
	Given <what> exists <where>
	Then the system knows when <what> was initially created, and who created it
	When I commit a modification to <what> at time t
	Then the system uses the new value of <what> for all purposes except change browsing and change comparison
	And the system knows that <what> changed at time t
	And the system knows that I am the person who committed the unit of work at time t
	And the system knows other users sharing the view at the time of the commit (if dynamic conflict resolution)
	And the system stores or can reconstruct the version <what> just before the commit at t 
	And the system stores or can reconstruct any earlier version <what> before the commit at t

	Examples:
	  | what                    | where                                                                 |
	  | description             | UseCase "Consumer Portal" scenario "Receive Real Time Pricing Update" |
	  | ontology.uml_model_URL  | System "Building Automation System"                                   |
	
# Alternative: could have 2-level tree table, where top level rows represent units of work,
# and the second level represents a change to one object within that unit of work
Scenario: Browse changes
	Given I have selected one or more objects
	Then I can examine a table of changes to those objects
	And a row describes a change made to one object
	And a column identifies the object
	And a column identifies the user who made the change
	And one or more columns describe the changes
	And a column shows when the change was made
	And I can filter rows based on the contents of any column
	And I can reorder the sequence of columns
	And I can hide columns I'm not interested in
	And I can sort based on the contents of any column
	And I can select two versions, and run "Compare versions"

# Alternative: could generate two text versions, and diff them.
Scenario: Compare versions
    Given I have selected two versions of one or more objects in "Browse changes"
    Then I am presented with a table of changes
    And a row shows the changes of each attribute (except for large text fields)
    And a column identifies the object
    And a column identifies the attribute
    And a column describes the first value
    And a column describes the second value
    And a list of large-text-field attributes is present
    And you can click on a row to see a diff of that text
    